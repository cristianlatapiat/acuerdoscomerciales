﻿using Autofac;
using Autofac.Integration.WebApi;
using LV.AcuerdosComerciales.Data;
using LV.AcuerdosComerciales.Data.Interfaces;
using LV.AcuerdosComerciales.Data.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace LV.AcuerdosComerciales.API
{
    public class AutofacWebapiConfig
    {

        public static IContainer Container;

        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }


        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            //Register your Web API controllers.  
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<AcuerdosDbContex>()
                   .As<DbContext>()
                   .InstancePerRequest();

            builder.RegisterType<DbFactory>()
                   .As<IDbFactory>()
                   .InstancePerRequest();

            builder.RegisterGeneric(typeof(GenericRepository<>))
                   .As(typeof(IGenericRepository<>))
                   .InstancePerRequest();

            builder.RegisterType<ActivosLVRepository>()
                .As<IActivosLV>()
                .InstancePerRequest();

            builder.RegisterType<ComisionRepository>()
               .As<IComision>()
               .InstancePerRequest();

            builder.RegisterType<ReportRepository>()
               .As<IReport>()
               .InstancePerRequest();

            builder.RegisterType<ManagerRepository>()
              .As<IManager>()
              .InstancePerRequest();

            builder.RegisterType<AcuerdoRepository>()
             .As<IAcuerdo>()
             .InstancePerRequest();


            //Set the dependency resolver to be Autofac.  
            Container = builder.Build();

            return Container;
        }

    }
}