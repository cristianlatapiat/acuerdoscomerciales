﻿using LV.AcuerdosComerciales.Data;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace LV.AcuerdosComerciales.API.Controllers
{
    [RoutePrefix("ACTIVOSLV")]
    public class ActivosController : ApiController
    {
        private readonly IActivosLV repository;

        public ActivosController(IActivosLV repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        [Route("LIST/{id}")]
        [ResponseType(typeof(IEnumerable<ActivoMetaData>))]
        public IHttpActionResult GetByFilter(int id)
        {
            SelectFilter filter = (SelectFilter)id;
            var managers = repository.GetByFilters(filter);
            return Ok(managers);
        }

        [HttpPost]
        [Route("LIST")]
        [ResponseType(typeof(IEnumerable<ActivoMetaData>))]
        public IHttpActionResult GetByCodes(List<string> codigos)
        {
            SelectFilter filter = SelectFilter.Todos;
            var managers = repository.GetByFilters(filter, codigos);
            return Ok(managers);
        }
    }
}
