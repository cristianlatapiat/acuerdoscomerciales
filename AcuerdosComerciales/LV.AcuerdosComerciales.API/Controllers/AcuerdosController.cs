﻿using LV.AcuerdosComerciales.API.Services;
using LV.AcuerdosComerciales.Data;
using LV.AcuerdosComerciales.Data.DTO;
using LV.AcuerdosComerciales.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace LV.AcuerdosComerciales.API.Controllers
{
    [RoutePrefix("ACUERDO")]
    public class AcuerdosController : ApiController
    {
        private readonly IGenericRepository<tbAcuerdo> repository;
        private readonly IAcuerdo acuerdoRepository;
        private readonly CalculoServices calculoServices;
        private readonly IActivosLV repositoryActivo;

        public AcuerdosController(IGenericRepository<tbAcuerdo> repository, IAcuerdo acuerdoRepository, IComision comisionRepository, IActivosLV repositoryActivo)
        {
            this.repository = repository;
            this.acuerdoRepository = acuerdoRepository;
            calculoServices = new CalculoServices(comisionRepository, acuerdoRepository);
            this.repositoryActivo = repositoryActivo;
        }

        [HttpGet]
        [Route("LIST")]
        [ResponseType(typeof(IEnumerable<tbAcuerdo>))]
        public IHttpActionResult GetAll(string id = "")
        {
            IEnumerable<AcuerdoModel> acuerdos;
            int idManager;
            if (!string.IsNullOrEmpty(id) && int.TryParse(id, out idManager))
            {
                acuerdos = repository.GetAll().Where(m => m.IdManager == idManager).AsEnumerable().Select(a => new AcuerdoModel(a));
                return Ok(acuerdos);
            }
            acuerdos = repository.GetAll().AsEnumerable().Select(a => new AcuerdoModel(a));
            return Ok(acuerdos);
        }



        /// <summary>
        /// Al editar un registro se transforma en un nuevo registro
        /// </summary>
        /// <param name="acuerdoNew"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("EDIT/{id}")]
        [ResponseType(typeof(tbAcuerdo))]
        public IHttpActionResult Create(int id, tbAcuerdo acuerdoNew)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // actualizo la FechaVigenciaHasta del registro actual

            var acuerdoOld = repository.GetAll().FirstOrDefault(r => r.Id == id);
            if (null == acuerdoOld)
            {
                return NotFound();
            }

            DateTime fecha = DateTime.Now;
            acuerdoOld.FechaHoraCambio = fecha;
            acuerdoOld.FechaVigenciaHasta = fecha;
            repository.Update(acuerdoOld);

            // ingreso el nuevo acuerdo
            acuerdoNew.FechaHoraCambio = fecha;
            repository.Add(acuerdoNew);

            DateTime desde = fecha.AddMonths(-4);
            var acuerdo = new AcuerdoModel(acuerdoNew);
           
            if (!acuerdo.FechaVigenciaHasta.HasValue && acuerdo.RebateAnual.HasValue)
            {
                // luego de ingresar el nuevo acuerdo se reprocesa hasta 4 meses
                calculoServices.CalculoComision(desde, fecha, new List<AcuerdoModel> { acuerdo });
            }

            return CreatedAtRoute("GetAcuerdoById", new { id = acuerdoNew.Id }, acuerdo);

        }

        [HttpGet]
        [Route("VIEW/{id}", Name = "GetAcuerdoById")]
        [ResponseType(typeof(tbAcuerdo))]
        public IHttpActionResult GetById(int id)
        {
            var acuerdo = repository.GetById(id);
            if (null == acuerdo)
            {
                return NotFound();
            }

            return Ok(new AcuerdoModel(acuerdo));
        }


        [HttpPost]
        [Route("RELACIONAR")]
        [ResponseType(typeof(tbAcuerdo))]
        public IHttpActionResult Relacionar(AcuerdoList acuerdos)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var acuerdosRelacionados = repositoryActivo.GetByFilters(SelectFilter.Asignados).Select(a => a.CODIGO_PRODUCTO);

            // actualizar agregar los acuerdos existentes y agregar acuerdos nuevos
            acuerdoRepository.Relacionar(acuerdos.Acuerdos.ToList());

            // se reprocesan los acuerdos ya registrados en el sistema 4 meses hacia atras
            var acuerdosAReprocesar = acuerdos.Acuerdos.Where(a => !a.FechaVigenciaHasta.HasValue && a.RebateAnual.HasValue && acuerdosRelacionados.Contains(a.CODIGO_PRODUCTO)).Select(a => new AcuerdoModel(a));

            if (acuerdosAReprocesar.Any())
            {
                DateTime hasta = DateTime.Now;
                DateTime desde  = hasta.AddMonths(-4);
                // luego de ingresar el nuevo acuerdo se reprocesa hasta 4 meses
                calculoServices.CalculoComision(desde, hasta, acuerdosAReprocesar);
            }

            return Ok();
        }

        [HttpGet]
        [Route("LISTHISTO/{codigoProducto}")]
        [ResponseType(typeof(IEnumerable<AcuerdoModel>))]
        public IHttpActionResult Historicos(string codigoProducto)
        {

            var historico = acuerdoRepository.GetHistorico(codigoProducto);

            return Ok(historico);
        }




    }
}
