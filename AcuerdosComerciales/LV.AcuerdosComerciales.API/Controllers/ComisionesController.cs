﻿using LV.AcuerdosComerciales.API.Services;
using LV.AcuerdosComerciales.Data;
using LV.AcuerdosComerciales.Data.DTO;
using LV.AcuerdosComerciales.Data.Interfaces;
using LV.AcuerdosComerciales.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;

namespace LV.AcuerdosComerciales.API.Controllers
{
    [RoutePrefix("COMISIONES")]
    public class ComisionesController : ApiController
    {
        private readonly IGenericRepository<tbComisionDiaria> repository;
        private readonly IAcuerdo acuerdoRepository;
        private readonly IComision comisionRepository;
        private readonly CalculoServices calculoServices;


        public ComisionesController(IGenericRepository<tbComisionDiaria> repository, IComision comisionRepository, IAcuerdo acuerdoRepository)
        {
            this.repository = repository;
            this.comisionRepository = comisionRepository;
            this.acuerdoRepository = acuerdoRepository;
            this.calculoServices = new CalculoServices(comisionRepository, acuerdoRepository);
        }

        [HttpGet]
        [Route("PATRIMONIO/{fecha}")]
        public IHttpActionResult GeAll(DateTime fecha)
        {
            // verificar
            var comisiones = repository.GetAll()
                           .Where(p => DbFunctions.TruncateTime(p.FechaHoraCalculo) == fecha)
                           .AsEnumerable().Select(c => new ComisionDiariaDTO(c));
            return Ok(comisiones);
        }

        /// <summary>
        /// Realiza el calculo de comisiones
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CALCULA")]
        public IHttpActionResult Calcular(DateTime? fecha = null)
        {

            if (!fecha.HasValue)
                fecha = DateTime.Now;
            calculoServices.CalculoComision(fecha.Value, fecha.Value);

            return Ok();

        }

        [HttpPost]
        [Route("CALCULA/{desde}")]
        public IHttpActionResult CalcularDesde(DateTime desde)
        {
            DateTime hasta = DateTime.Now;

            calculoServices.CalculoComision(desde, hasta);

            return Ok();

        }


    }
}
