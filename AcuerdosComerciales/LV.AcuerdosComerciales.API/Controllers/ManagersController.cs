﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using LV.AcuerdosComerciales.Data;
using LV.AcuerdosComerciales.Data.DTO;
using System.Collections.Generic;
using System.Web.Http.Description;
using System;
using LV.AcuerdosComerciales.Data.Interfaces;
using System.Globalization;

namespace LV.AcuerdosComerciales.API.Controllers
{
    [RoutePrefix("MANAGER")]
    public class ManagersController : ApiController
    {
        private readonly IGenericRepository<tbManager> repository;
        private readonly IManager managerRepository;


        public ManagersController(IGenericRepository<tbManager> repository, IManager managerRepository )
        {
            this.repository = repository;
            this.managerRepository = managerRepository;
        }

        [HttpGet]
        [Route("LIST")]
        [ResponseType(typeof(IEnumerable<ManagerDTO>))]
        public IHttpActionResult GetAll()
        {
            var managers = managerRepository.Get();
            return Ok(managers);
        }

        [HttpGet]
        [Route("VIEW/{id}", Name = "GetManagerById")]
        [ResponseType(typeof(ManagerDTO))]
        public IHttpActionResult GetById(int id)
        {
            var managers = repository.GetById(id);
            if (null == managers)
            {
                return NotFound();
            }
            return Ok(new ManagerDTO(managers));
        }

        [HttpPost]
        [Route("NEW")]
        [ResponseType(typeof(ManagerDTO))]
        public IHttpActionResult Create(ManagerDTO manager)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            tbManager tbManager = new tbManager() {
                Contacto = manager.Contacto,
                DocumentosGuid = Guid.NewGuid().ToString(),
                Email = manager.Email,
                Nombre = manager.Nombre,
                Plataforma = manager.Plataforma,
                Telefono = manager.Telefono
            };
            DateTime fecha;
            string[] formats = { "dd/MM/yyyy", "dd-MM-yyyy"};
            if (manager.FechaContrato != "" && DateTime.TryParseExact(manager.FechaContrato, formats,CultureInfo.InvariantCulture,DateTimeStyles.None, out fecha))
            {
                tbManager.FechaContrato = fecha;
            }

            repository.Add(tbManager);

            return CreatedAtRoute("GetManagerById", new { id = manager.Id }, new ManagerDTO(tbManager));

        }

        [HttpPut]
        [Route("EDIT/{id}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult Update(int id, ManagerDTO manager)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != manager.Id)
            {
                return BadRequest();
            }

            tbManager tbManager = new tbManager()
            {
                Id = id,
                Contacto = manager.Contacto,
                Email = manager.Email,
                Nombre = manager.Nombre,
                Plataforma = manager.Plataforma,
                Telefono = manager.Telefono,
                DocumentosGuid = manager.DocumentosGuid
            };
            DateTime fecha;
            string[] formats = { "dd/MM/yyyy", "dd-MM-yyyy" };
            if (manager.FechaContrato != "" && DateTime.TryParseExact(manager.FechaContrato, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out fecha))
            {
                tbManager.FechaContrato = fecha;
            }


            try
            {
                repository.Update(tbManager);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ObjectExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);

        }

        [HttpDelete]
        [Route("DELETE/{id}")]
        [ResponseType(typeof(ManagerDTO))]
        public IHttpActionResult Delete(int id)
        {
            tbManager tbManager = repository.GetById(id);
            if (tbManager == null)
            {
                return NotFound();
            }

            managerRepository.DeleteManager(id);

            return Ok(new ManagerDTO(tbManager));
        }

        private bool ObjectExists(int id)
        {
            return repository.GetAll().Any(m => m.Id == id);
        }
    }
}