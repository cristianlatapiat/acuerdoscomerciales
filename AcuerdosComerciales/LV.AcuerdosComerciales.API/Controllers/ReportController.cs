﻿using LV.AcuerdosComerciales.Data.Interfaces;
using System.Web.Http;

namespace LV.AcuerdosComerciales.API.Controllers
{
    [RoutePrefix("REPORTE")]
    public class ReportController : ApiController
    {
        private readonly IReport repository;


        public ReportController(IReport repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        [Route("DIARIOMANFONDO/{periodo}/{idManager}")]
        public IHttpActionResult GetDiario(string periodo, int idManager, string fondo = "")
        {
            var managers = repository.GetDiario(periodo, idManager, fondo);
            return Ok(managers);
        }



        [HttpGet]
        [Route("RESUMENMANAGER/{periodo}")]
        public IHttpActionResult GetResumen(string periodo)
        {
            var managers = repository.GetResumen(periodo);
            return Ok(managers);
        }


        [HttpGet]
        [Route("PERIODOS")]
        public IHttpActionResult GetPeriodos()
        {
            var managers = repository.GetPeriodos();
            return Ok(managers);
        }

    }
}
