﻿using LV.AcuerdosComerciales.Data.DTO;
using LV.AcuerdosComerciales.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace LV.AcuerdosComerciales.API.Services
{
    public class CalculoServices
    {
        private readonly IComision comisionRepository;
        private readonly IAcuerdo acuerdoRepository;
        public CalculoServices(IComision comisionRepository, IAcuerdo acuerdoRepository)
        {
            this.comisionRepository = comisionRepository;
            this.acuerdoRepository = acuerdoRepository;
        }
        public void CalculoComision(DateTime desde, DateTime hasta, IEnumerable<AcuerdoModel> acuerdosACalcular = null )
        {

            IEnumerable<AcuerdoModel> acuerdos = acuerdosACalcular;
            if (!acuerdos.Any())
            {
                acuerdos = acuerdoRepository.GetByDateRange(desde, hasta);
            }

            XElement xml = new XElement("Fondos", acuerdos.Select(a => a.Serializar()));
            
            // buscar los patrimonios por rango de fechas
            var patrimonioList = comisionRepository.Get(xml.ToString(), desde, hasta);

            // SI EXISTE INFORMACION DE PATRIMONIO SE GUARDA 
            if (patrimonioList.Any())
            {
                comisionRepository.CalculoComision(patrimonioList);
            }

        }

        public IEnumerable<DateTime> EachDay(DateTime desde, DateTime hasta)
        {
            for (var day = desde.Date; day.Date <= hasta.Date; day = day.AddDays(1))
                yield return day;
        }
    }
}