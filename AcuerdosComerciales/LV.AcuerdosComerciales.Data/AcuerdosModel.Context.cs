﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LV.AcuerdosComerciales.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class AcuerdosDbContex : DbContext
    {
        public AcuerdosDbContex()
            : base("name=AcuerdosDbContex")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ActivosLV> ActivosLV { get; set; }
        public virtual DbSet<tbAcuerdo> tbAcuerdo { get; set; }
        public virtual DbSet<tbComisionDiaria> tbComisionDiaria { get; set; }
        public virtual DbSet<tbManager> tbManager { get; set; }
    
        public virtual int sp_AcuerdoComercial_CalculoComision(string fondos)
        {
            var fondosParameter = fondos != null ?
                new ObjectParameter("fondos", fondos) :
                new ObjectParameter("fondos", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_AcuerdoComercial_CalculoComision", fondosParameter);
        }
    }
}
