﻿using System;

namespace LV.AcuerdosComerciales.Data
{

    public class ActivoMetaData : ActivosLV
    {
        public int? IdAcuerdo { get; set; }
        public string NombreManager { get; set; }
        public string PortafolioCarteraModelo { get; set; }
        public string DescipcionTipoPago { get; set; }
        public string DescipcionTipoRebate { get; set; }
        public decimal? ManagementFree { get; set; }
        public decimal? RebateAnual { get; set; }
        public decimal? AcuerdoComercial { get; set; }
        public string FechaVigenciaDesde { get; set; }
        public int? IdManager { get; set; }
        public string CodigoInterno { get; set; }
        public string TipoPago { get; set; }
        public string TipoRebate { get; set; }

    }
}
