﻿using System.Collections.Generic;

namespace LV.AcuerdosComerciales.Data
{
    public class AcuerdoList
    {
        public IEnumerable<tbAcuerdo> Acuerdos { get; set; }
    }
}
