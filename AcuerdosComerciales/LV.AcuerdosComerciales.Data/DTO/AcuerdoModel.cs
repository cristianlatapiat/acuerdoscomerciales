﻿using LV.AcuerdosComerciales.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LV.AcuerdosComerciales.Data.DTO
{
    public class AcuerdoModel
    {
        public int Id { get; set; }
        public int IdManager { get; set; }
        public string CODIGO_PRODUCTO { get; set; }
        public DateTime? FechaVigenciaDesde { get; set; }
        public decimal? RebateAnual { get; set; }
        public string TipoPago { get; set; }
        public string TipoRebate { get; set; }
        public decimal? ManagementFree { get; set; }
        public decimal? AcuerdoComercial { get; set; }
        public DateTime? FechaHoraCambio { get; set; }
        public string UsuarioReg { get; set; }
        public string NemoBaseComercial { get; set; }
        public string CodigoInterno { get; set; }
        public string IdDocumentos { get; set; }

        public XElement Serializar()
        {
            return new XElement("Fondo",
                new XAttribute("Codigo", this.CODIGO_PRODUCTO)
               );
        }

        public DateTime? FechaVigenciaHasta { get; set; }
        public string Manager { get; set; }
        public string DescipcionPago { get; set; }
        public string DescripcionRebate { get; set; }

        public AcuerdoModel()
        {

        }

        public AcuerdoModel(tbAcuerdo acuerdo)
        {
            AcuerdoComercial = acuerdo.AcuerdoComercial;
            CodigoInterno = acuerdo.CodigoInterno;
            CODIGO_PRODUCTO = acuerdo.CODIGO_PRODUCTO;
            FechaHoraCambio = acuerdo.FechaHoraCambio;
            FechaVigenciaDesde = acuerdo.FechaVigenciaDesde;
            FechaVigenciaHasta = acuerdo.FechaVigenciaHasta;
            Id = acuerdo.Id;
            IdDocumentos = acuerdo.IdDocumentos;
            IdManager = acuerdo.IdManager;
            ManagementFree = acuerdo.ManagementFree;
            NemoBaseComercial = acuerdo.NemoBaseComercial;
            RebateAnual = acuerdo.RebateAnual;
            TipoPago = acuerdo.TipoPago;
            TipoRebate = acuerdo.TipoRebate;
            UsuarioReg = acuerdo.UsuarioReg;
        }
    }
}
