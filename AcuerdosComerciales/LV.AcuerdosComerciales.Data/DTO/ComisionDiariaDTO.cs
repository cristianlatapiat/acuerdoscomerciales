﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV.AcuerdosComerciales.Data.DTO
{
    public class ComisionDiariaDTO
    {
        public int Id { get; set; }
        public int IdAcuerdo { get; set; }
        public decimal PatrimonioFondo { get; set; }
        public decimal Comision { get; set; }
        public DateTime FechaHoraCalculo { get; set; }


        public ComisionDiariaDTO(tbComisionDiaria comision)
        {
            Id = comision.Id;
            IdAcuerdo = comision.IdAcuerdo;
            PatrimonioFondo = comision.PatrimonioFondo;
            FechaHoraCalculo = comision.FechaHoraCalculo;
        }
    }
}
