﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV.AcuerdosComerciales.Data.DTO
{
    public class ManagerDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Plataforma { get; set; }
        public string Contacto { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string FechaContrato { get; set; }
        public string DocumentosGuid { get; set; }
        public string TieneAcuerdos { get; set; }
        public string PlataformaName { get; set; }

        public ManagerDTO()
        {

        }
        public ManagerDTO(tbManager manager)
        {
            Id = manager.Id;
            Nombre = manager.Nombre;
            Plataforma = manager.Plataforma;
            PlataformaName = manager.Plataforma == "L" ? "Local" : "Internacional";
            Contacto = manager.Contacto;
            Email = manager.Email;
            Telefono = manager.Telefono;
            if (manager.FechaContrato.HasValue)
                FechaContrato = manager.FechaContrato.Value.ToString("dd-MM-yyyy");
            DocumentosGuid = manager.DocumentosGuid;
        }
    }
}
