﻿namespace LV.AcuerdosComerciales.Data.DTO
{
    public class ManagerFondoReport
    {

        public string Fondo { get; set; }
        public decimal? ManagementFree { get; set; }
        public decimal? RebateAnual { get; set; }
        public string Fecha { get; set; }
        public decimal? TotalManagementFree { get; set; }
        public decimal? TotalRebateAnual { get; set; }
        public decimal? ManagementFreeDiario { get; set; }
        public decimal? RebateDiario { get; set; }

        public int IdAcuerdo { get; set; }
        public decimal PatrimonioFondo { get; set; }
        public decimal Comision { get; set; }
        public string NombreManager { get; set; }
        public string Periodo { get; set; }


    }
}
