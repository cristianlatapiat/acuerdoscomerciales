﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV.AcuerdosComerciales.Data.DTO
{
    public class ResumenManger
    {
        public string NombreManager { get; set; }
        public string Fondo { get; set; }
        public decimal? ManagementFree { get; set; }
        public decimal? RebateAnual { get; set; }
        public string TipoRebate { get; set; }
        public decimal? TotalManagementFree { get; set; }
        public decimal? TotalRebateAnual { get; set; }
        public decimal? SumaTotalManagementFree { get; set; }
        public decimal? SumaTotalRebateAnual { get; set; }
        public string Periodo { get; set; }


    }
}
