﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV.AcuerdosComerciales.Data
{
    public class DbFactory : IDbFactory
    {
        AcuerdosDbContex dbContext;
        

        public AcuerdosDbContex Init()
        {
            return dbContext ?? (dbContext = new AcuerdosDbContex());
        }
    }
}
