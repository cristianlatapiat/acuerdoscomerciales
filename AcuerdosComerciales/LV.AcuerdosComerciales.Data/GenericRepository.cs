﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace LV.AcuerdosComerciales.Data
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {

        private AcuerdosDbContex dbContext;

        protected IDbFactory DbFactory
        {
            get;
            private set;
        }

        protected AcuerdosDbContex DbContext
        {
            get { return dbContext ?? (dbContext = DbFactory.Init()); }
        }

        public GenericRepository(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        public IQueryable<T> GetAll()
        {
            return DbContext.Set<T>();
        }

        public void Add<T>(T model) where T : class
        {
            DbContext.Set<T>().Add(model);
            DbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var model = DbContext.Set<T>().Find(id);
            DbContext.Set<T>().Remove(model);
            DbContext.SaveChanges();
        }

        public T GetById(int id)
        {
            return DbContext.Set<T>().Find(id);
        }

        public void Update<T>(T model) where T : class
        {
            DbContext.Set<T>().Attach(model);
            DbContext.Entry(model).State = EntityState.Modified;
            DbContext.SaveChanges();
        }

        public void AddRange<T>(IList<T> entities) where T : class
        {
            DbContext.Set<T>().AddRange(entities);
            DbContext.SaveChanges();
        }
    }
}
