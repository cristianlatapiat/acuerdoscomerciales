﻿using System.Collections.Generic;
using System.Linq;

namespace LV.AcuerdosComerciales.Data
{
    public interface IGenericRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        void Add<T>(T model) where T : class;
        void Delete(int id);
        T GetById(int id);
        void Update<T>(T model) where T : class;
        void AddRange<T>(IList<T> entities) where T : class;
    }
}
