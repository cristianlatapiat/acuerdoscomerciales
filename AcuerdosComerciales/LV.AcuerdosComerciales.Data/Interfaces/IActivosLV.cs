﻿using System.Collections.Generic;

namespace LV.AcuerdosComerciales.Data
{
    public interface IActivosLV
    {
        IEnumerable<ActivoMetaData> GetByFilters(SelectFilter filter, IEnumerable<string> codigosList = null);
    }

    public enum SelectFilter
    {
       Todos,
       SinAsignar,
       Asignados,
       Activos,
       Inactivos,
       EnPortafolio
    }
}
