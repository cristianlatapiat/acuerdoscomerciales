﻿using LV.AcuerdosComerciales.Data.DTO;
using System;
using System.Collections.Generic;

namespace LV.AcuerdosComerciales.Data.Interfaces
{
    public interface IAcuerdo
    {
        void Relacionar(IEnumerable<tbAcuerdo> acuerdos);
        IEnumerable<AcuerdoModel> GetHistorico(string codigoProducto);
        IEnumerable<AcuerdoModel> GetByDateRange(DateTime desde, DateTime hasta, int? idAcuerdo = null);
    }
}
