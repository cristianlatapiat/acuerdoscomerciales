﻿using LV.AcuerdosComerciales.Data.Models;
using System;
using System.Collections.Generic;

namespace LV.AcuerdosComerciales.Data.Interfaces
{
    public interface IComision
    {
        IEnumerable<PatrimonioModel> Get(string fondosXml, DateTime fechaDesde, DateTime fechaHasta);
        void DeleteByDate(DateTime fecha);
        void CalculoComision(IEnumerable<PatrimonioModel> patrimonioList);
    }
}
