﻿using LV.AcuerdosComerciales.Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV.AcuerdosComerciales.Data.Interfaces
{
    public interface IManager
    {
        void DeleteManager(int id);
        IEnumerable<ManagerDTO> Get();
    }
}
