﻿using LV.AcuerdosComerciales.Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV.AcuerdosComerciales.Data.Interfaces
{
    public interface IReport
    {
        IEnumerable<ManagerFondoReport> GetDiario(string periodo, int idManager, string fondo);

        IEnumerable<ResumenManger> GetResumen(string periodo);
        IEnumerable<int> GetPeriodos();
    }
}
