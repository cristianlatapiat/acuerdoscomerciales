﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LV.AcuerdosComerciales.Data.Models
{
    public class PatrimonioModel
    {
        public decimal? Patrimonio { get; set; }
        public string CODIGO_PRODUCTO { get; set; }
        public int? fecha { get; set; }

        internal XElement Serializar()
        {
            DateTime fechaPatrimonio;
            DateTime.TryParseExact(fecha.ToString(), "yyyyMMdd",CultureInfo.InvariantCulture,DateTimeStyles.None, out fechaPatrimonio);
          
            return new XElement("Fondo", 
                 new XAttribute("Codigo", this.CODIGO_PRODUCTO),
                 new XAttribute("Patrimonio", this.Patrimonio),
                 new XAttribute("Fecha", fechaPatrimonio.ToString("dd-MM-yyyy"))
                );
        }
    }
}
