﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LV.AcuerdosComerciales.Data
{
    [MetadataType(typeof(ManagerMetaData))]
    public partial class tbManager
    {
       
    }
    public class ManagerMetaData
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(250)]
        public string Nombre { get; set; }
        [Required]
        [MaxLength(1)]
        public string Plataforma { get; set; }
        [MaxLength(250)]
        public string Contacto { get; set; }
        [MaxLength(250)]
        public string Email { get; set; }

        [MaxLength(50)]
        public string Telefono { get; set; }

        public DateTime? FechaContrato { get; set; }
        public string DocumentosGuid { get; set; }
    }
}
