﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace LV.AcuerdosComerciales.Data
{
    public class ActivosLVRepository : SqlConnectionManager, IActivosLV
    {

        public IEnumerable<ActivoMetaData> GetByFilters(SelectFilter filter, IEnumerable<string> codigosList = null)
        {
            var select = @"SELECT B.Id as IdAcuerdo
                        , CASE B.TipoPago
	                        WHEN 'T' THEN 'Trimestral'
	                        WHEN 'A' THEN 'Anual'
	                        WHEN 'M' THEN 'Mensual'  end as DescipcionTipoPago
                        , CASE  B.TipoRebate
	                        WHEN 'V' THEN 'Variable'
	                        WHEN 'F' THEN 'Fijo'  end as DescipcionTipoRebate
                        , B.TipoPago
                        , B.TipoRebate
                        , B.ManagementFree
                        , B.RebateAnual
                        , B.AcuerdoComercial
                        , replace(convert(VARCHAR,B.FechaVigenciaDesde,103),'/','-') as FechaVigenciaDesde
                        , B.IdManager
                        , B.CodigoInterno
                        , A.CODIGO_PRODUCTO
                        , A.ISIN	
                        , A.TipoProducto
                        , case when  A.NombreComercial <> '' then  A.NombreComercial else  A.NOMBRE_ACTIVO end as NOMBRE_ACTIVO
                        , M.Nombre as NombreManager
                        ,CASE WHEN I.Cantidad = 0 THEN 'NO' ELSE 'SI' END AS PortafolioCarteraModelo 
                        FROM [ActivosLV] A 
                        LEFT JOIN tbAcuerdo B ON A.CODIGO_PRODUCTO= B.CODIGO_PRODUCTO 
                        LEFT JOIN  tbManager M ON B.IdManager = M.Id
                        LEFT JOIN (SELECT COUNT (InstrumentosPortLV_ID) Cantidad,ISIN FROM InstrumentosPortLV GROUP BY ISIN) I ON  A.[ISIN] = I.[ISIN]
                        where
                        rtrim(ltrim(A.TipoProducto)) in ('Fondos Mutuos','Fondos de Inversión','ETFs') 
                        and A.CODIGO_PRODUCTO <> ''  and B.FechaVigenciaHasta is null ";

            if (codigosList != null && codigosList.Any())
            {
                var codigos = string.Join("','", codigosList);
                select += " and A.CODIGO_PRODUCTO in('" + codigos + "') ";
            }

            switch (filter)
            {

                case SelectFilter.SinAsignar:
                    select += " AND  M.Nombre IS NULL ";
                    break;
                case SelectFilter.Asignados:
                    select += " AND  M.Nombre IS NOT NULL ";
                    break;
                case SelectFilter.Activos:
                    select += " AND  A.Activo = 1 ";
                    break;
                case SelectFilter.Inactivos:
                    select += " AND ( A.Activo = 0 OR A.Activo IS NULL) ";
                    break;
                case SelectFilter.EnPortafolio:
                    select += " AND I.Cantidad >0 ";
                    break;
                default:
                    select += " ";
                    break;
            }
            using (var connection = CreateConnection())
            {
                return connection.Query<ActivoMetaData>(select);
            }

        }
    }
}
