﻿using Dapper;
using LV.AcuerdosComerciales.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using LV.AcuerdosComerciales.Data.DTO;

namespace LV.AcuerdosComerciales.Data.Repository
{
    public class AcuerdoRepository : SqlConnectionManager, IAcuerdo
    {
        public IEnumerable<AcuerdoModel> GetByDateRange(DateTime desde, DateTime hasta, int? idAcuerdo = null)
        {
            var sqlSelect = @" SET DATEFORMAT dmy;
                            select Id
	                        , IdManager
	                        , CODIGO_PRODUCTO
	                        , FechaVigenciaDesde
	                        , RebateAnual
	                        , TipoPago
	                        , TipoRebate
	                        , ManagementFree
	                        , AcuerdoComercial
	                        , FechaHoraCambio
	                        , UsuarioReg
	                        , NemoBaseComercial
	                        , CodigoInterno
	                        , IdDocumentos
	                        , FechaVigenciaHasta 
                        from tbAcuerdo with(nolock)
                        where 
                        FechaVigenciaHasta is null and
                        cast( convert(varchar(10), FechaVigenciaDesde, 103)  as datetime) 
                        between @fechaDesde and @fechaHasta
                        and RebateAnual is not null ";

            if (idAcuerdo.HasValue)
            {
                sqlSelect += " and Id = @idAcuerdo";
            }

            using (var connection = CreateConnection())
            {
                return connection.Query<AcuerdoModel>(sqlSelect, new { fechaDesde = desde.ToString("dd-MM-yyyy"), fechaHasta = hasta.ToString("dd-MM-yyyy"), idAcuerdo });
            }
        }

        public IEnumerable<AcuerdoModel> GetHistorico(string codigoProducto)
        {
            var sqlSelect = @"SELECT A.Id
                              ,M.Nombre as Manager
                              ,A.CODIGO_PRODUCTO
                              ,A.FechaVigenciaDesde
                              ,A.RebateAnual
                              ,CASE A.TipoPago WHEN  'A' THEN 'Anual'
		                        WHEN  'T' THEN 'Trimestral' ELSE 'Mensual' END AS DescipcionPago
                              , CASE A.TipoRebate WHEN 'F' THEN 'Fijo' else 'Variable'end as DescripcionRebate
                              ,A.ManagementFree
                              ,A.AcuerdoComercial
                              ,A.FechaHoraCambio
                              ,A.UsuarioReg
                              ,A.NemoBaseComercial
                              ,A.CodigoInterno
                              ,A.IdDocumentos
                              ,A.FechaVigenciaHasta
                          FROM tbAcuerdo A with(nolock)
                          inner join tbManager  M  with(nolock) ON A.IdManager = m.Id
                          where 
                          CODIGO_PRODUCTO = @codigo 
                          and FechaVigenciaHasta is not null ";

            using (var connection = CreateConnection())
            {
                return connection.Query<AcuerdoModel>(sqlSelect, new { codigo = codigoProducto });
            }
        }

        public void Relacionar(IEnumerable<tbAcuerdo> acuerdos)
        {
            string codigos =  string.Join("','", acuerdos.Select(a => a.CODIGO_PRODUCTO));
            // se actualizan los acuerdos existentes
            string updateExist = @"update tbAcuerdo
                                set FechaHoraCambio =getdate(),
                                FechaVigenciaHasta = getdate()
                                where CODIGO_PRODUCTO in ('"  + codigos +"') and FechaVigenciaHasta is null";

            // se insertan los nuevos acuerdos
            string insertAcuerdos = @"INSERT INTO tbAcuerdo
                                       (IdManager, CODIGO_PRODUCTO, FechaVigenciaDesde
                                       , RebateAnual, TipoPago, TipoRebate, ManagementFree
                                       , AcuerdoComercial, FechaHoraCambio, UsuarioReg
                                       , NemoBaseComercial, CodigoInterno, IdDocumentos)
                                 VALUES
                                       (@IdManager, @CODIGO_PRODUCTO, getdate()
                                       , @RebateAnual, @TipoPago, @TipoRebate
                                       , @ManagementFree, @AcuerdoComercial, getdate()
                                       , @UsuarioReg, @NemoBaseComercial, @CodigoInterno
                                       , @IdDocumentos)";

            using (var connection = CreateConnection())
            {

                using (TransactionScope transactionScope = new TransactionScope())
                {
                    // se actualizan los registros existentes
                    connection.Execute(updateExist);

                    foreach (var acuerdo in acuerdos)
                    {
                        connection.Execute(insertAcuerdos, 
                            new {
                                IdManager = acuerdo.IdManager,
                                CODIGO_PRODUCTO = acuerdo.CODIGO_PRODUCTO,
                                RebateAnual = acuerdo.RebateAnual,
                                TipoPago = acuerdo.TipoPago,
                                TipoRebate = acuerdo.TipoRebate,
                                ManagementFree = acuerdo.ManagementFree,
                                AcuerdoComercial = acuerdo.AcuerdoComercial,
                                UsuarioReg = acuerdo.UsuarioReg,
                                NemoBaseComercial = acuerdo.NemoBaseComercial,
                                CodigoInterno = acuerdo.CodigoInterno,
                                IdDocumentos = acuerdo.IdDocumentos,
                            });
                    }

                    transactionScope.Complete();
                }
            }

        }
    }
}
