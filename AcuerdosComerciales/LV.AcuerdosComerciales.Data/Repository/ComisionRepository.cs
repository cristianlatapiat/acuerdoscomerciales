﻿using LV.AcuerdosComerciales.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LV.AcuerdosComerciales.Data.Models;
using System.Data.SqlClient;
using Dapper;
using System.Xml.Linq;

namespace LV.AcuerdosComerciales.Data.Repository
{
    public class ComisionRepository : IComision
    {
        public void CalculoComision(IEnumerable<PatrimonioModel> patrimonioList)
        {
            var xmlFondos = new XElement("Fondos", patrimonioList.Select(p => p.Serializar()));
            using (var ctx = new AcuerdosDbContex())
            {
                ctx.sp_AcuerdoComercial_CalculoComision(xmlFondos.ToString());
            }
        }

        public void DeleteByDate(DateTime fecha)
        {
            var delete = "DELETE FROM tbComisionDiaria WHERE convert(varchar(10), [FechaHoraCalculo], 23) = @fecha";
            using (var ctx = new AcuerdosDbContex())
            {
                var connString = ctx.Database.Connection.ConnectionString;

                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Execute(delete, new { fecha = fecha.ToString("yyyy-MM-dd") });
                }

            }
        }

        public IEnumerable<PatrimonioModel> Get(string fondosXml, DateTime fechaDesde, DateTime fechaHasta)
        {
            IEnumerable<PatrimonioModel> result;

            using (var ctx = new LvInversionesEntities())
            {
                var fondos = new SqlParameter("@fondos", fondosXml);
                var desde = new SqlParameter("@fechaDesde", fechaDesde.ToString("dd-MM-yyyy"));
                var hasta = new SqlParameter("@fechaHasta", fechaHasta.ToString("dd-MM-yyyy"));
                result = ctx.Database.SqlQuery<PatrimonioModel>("sp_AcuerdoComercial_GetPatrimonio @fondos,@fechaDesde, @fechaHasta ", parameters: new[] { fondos, desde, hasta }).ToList();

            }

            return result;



        }


    }
}
