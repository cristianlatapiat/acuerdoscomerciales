﻿using LV.AcuerdosComerciales.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dapper;
using LV.AcuerdosComerciales.Data.DTO;

namespace LV.AcuerdosComerciales.Data.Repository
{
    public class ManagerRepository : SqlConnectionManager, IManager
    {
        public void DeleteManager(int id)
        {
            string deleteComisiones = @"delete tbComisionDiaria 
                                        from 
                                        tbComisionDiaria C
                                        INNER JOIN tbAcuerdo A ON C.IdAcuerdo = A.Id
                                        where A.IdManager = @IdManager";

            string deleteAcuerdo = "delete from tbAcuerdo where IdManager = @IdManager";
            string deleteManager = "delete from tbManager where id = @id ";

            using (var connection = CreateConnection())
            {
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    connection.Execute(deleteComisiones, new { IdManager = id });

                    connection.Execute(deleteAcuerdo, new { IdManager = id });

                    connection.Execute(deleteManager, new { id = id });

                    transactionScope.Complete();

                }
            }
        }

        public IEnumerable<ManagerDTO> Get()
        {
            var sqlSelect = @"select 
	                            M.Id, 
	                            M.Nombre, 
	                            CASE WHEN M.Plataforma = 'L' THEN 'Local' else 'Internacional' end as PlataformaName, 
                                M.Plataforma,
	                            M.Contacto, 
	                            M.Email, 
	                            M.Telefono, 
	                            replace(convert(VARCHAR,M.FechaContrato,103),'/','-') as FechaContrato, 
	                            M.DocumentosGuid ,
	                            (SELECT case when COUNT(*) > 0 then 'SI' else 'NO' end FROM tbAcuerdo WHERE IdManager = M.Id) AS TieneAcuerdos
                            from 
	                            tbManager M with(nolock)";

            using (var connection = CreateConnection())
            {
                return connection.Query<ManagerDTO>(sqlSelect);
            }
        }
    }
}
