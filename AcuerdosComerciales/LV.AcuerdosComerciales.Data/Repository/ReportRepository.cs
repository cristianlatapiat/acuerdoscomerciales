﻿using LV.AcuerdosComerciales.Data.Interfaces;
using System.Collections.Generic;
using LV.AcuerdosComerciales.Data.DTO;
using Dapper;

namespace LV.AcuerdosComerciales.Data.Repository
{
    public class ReportRepository : SqlConnectionManager, IReport
    {


        public IEnumerable<ManagerFondoReport> GetDiario(string periodo, int idManager, string fondo)
        {
            var selectSql = @"SELECT 
	                        A.CODIGO_PRODUCTO as Fondo,
	                        A.ManagementFree as ManagementFree,
	                        A.RebateAnual  as RebateAnual,
	                        convert(varchar(10), FechaHoraCalculo , 23) as Fecha,
	                        CASE WHEN A.ManagementFree IS NOT NULL THEN
	                        SUM(C.Comision) OVER(PARTITION BY IdAcuerdo) ELSE NULL END AS TotalManagementFree,
	                        CASE WHEN A.ManagementFree IS NOT NULL THEN
	                        C.Comision ELSE NULL END as ManagementFreeDiario,
	                        CASE WHEN A.RebateAnual IS NOT NULL THEN
	                        C.Comision ELSE NULL END as RebateDiario,
	                        CASE WHEN A.RebateAnual IS NOT NULL THEN
	                        SUM(C.Comision) OVER(PARTITION BY IdAcuerdo) ELSE NULL END  AS  TotalRebateAnual,
	                        IdAcuerdo, 
	                        PatrimonioFondo, 
	                        Comision,
	                        M.Nombre as NombreManager,
	                        replace( convert(varchar(7), Convert(datetime,Fecha,103), 126), '-','') as Periodo  
                            FROM tbComisionDiaria C with(nolock)
                            INNER JOIN tbAcuerdo A with(nolock) ON C.IdAcuerdo = A.Id and A.FechaVigenciaHasta is null
                            INNER JOIN tbManager M with(nolock) ON A.IdManager = M.Id
                            where A.IdManager = @idManager 
	                        and  replace( convert(varchar(7), Convert(datetime,Fecha,103), 126), '-','') = @periodo  ";

            if (!string.IsNullOrEmpty(fondo))
            {
                selectSql += " and A.CODIGO_PRODUCTO = @fondo ";
            }

            using (var connection = CreateConnection())
            {
                return connection.Query<ManagerFondoReport>(selectSql, new { periodo, idManager, fondo });
            }

        }

        public IEnumerable<ResumenManger> GetResumen(string periodo)
        {
            var selectSql = @"SELECT 
	                        Resultado.* ,
	                        SUM(Resultado.TotalManagementFree) OVER(PARTITION BY Resultado.IdManager)  AS  SumaTotalManagementFree,
	                        SUM(Resultado.TotalRebateAnual) OVER(PARTITION BY Resultado.IdManager)  AS  SumaTotalRebateAnual
                        FROM 
                        (
                        SELECT 
                            M.Nombre as NombreManager,
                            A.CODIGO_PRODUCTO as Fondo,
                            A.ManagementFree  as ManagementFree,
                            A.RebateAnual  as RebateAnual,
                            Case when A.TipoRebate = 'F' then 'Fijo' else 'Variable' end as TipoRebate,
	                        case when  A.ManagementFree is not null THEN
                            C.TotalComision  else null end as TotalManagementFree,
	                        CASE WHEN A.RebateAnual IS NOT NULL THEN
	                        C.TotalComision  ELSE NULL END AS TotalRebateAnual,
                            C.Periodo,
	                        M.Id as IdManager
                            FROM (
	                            select 
	                            sum(comision) as TotalComision, 
	                            IdAcuerdo ,
	                            replace( convert(varchar(7), Convert(datetime,Fecha,103), 126), '-','') as Periodo
	                            from tbComisionDiaria  with(nolock)
	                            group by 
		                            IdAcuerdo, 
		                            replace( convert(varchar(7), Convert(datetime,Fecha,103), 126), '-','')
                            ) as C 
                            INNER JOIN tbAcuerdo A with(nolock) ON C.IdAcuerdo = A.Id and A.FechaVigenciaHasta is null
                            INNER JOIN tbManager M with(nolock) ON A.IdManager = M.Id
                            where  
	                            C.Periodo = @periodo 
		                        ) AS Resultado ";


            using (var connection = CreateConnection())
            {
                return connection.Query<ResumenManger>(selectSql, new { periodo });
            }

        }

        public IEnumerable<int> GetPeriodos()
        {
            var selectSql = @"  select 
		                            distinct top 20 convert(int, replace( convert(varchar(7), Convert(datetime,Fecha,103), 126), '-','')) Periodos
                                from tbComisionDiaria with(nolock)
                                order by 1 desc ";


            using (var connection = CreateConnection())
            {
                return connection.Query<int>(selectSql);
            }

        }
    }
}
