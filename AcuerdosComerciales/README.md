## API REST Ws_AcuerdosComerciales
El Mantenedor de acuerdos comerciales, permite establecer una relación comercial con distintos managers, con el objetivo de distribuir los fondos que éstos ofrecen a sus clientes.Esta relación se ve reflejada en un acuerdo, que contiene el acuerdo comercial y de inversión.

### Tecnología

C#.Net Framework 4.5.2
Visual Studio 2015
Sqlserver 2014
Linq + Entity Framework

### Compilación

Setear por defecto el proyecto de Web LV.AcuerdosComerciales.API
Generar solución en modo release de proyecto

### Instalación

Copiar resultado de carpeta ~\AcuerdosComerciales\LV.AcuerdosComerciales.API\bin\Release
hacia Produccion
El archivo de configuración es Web.config

### Configuración en producción

Configurar archivo Web.config con Bases de datos productivas.

### Autor

* **Cristian Muñoz** - *Larrain Vial*