GO
/****** Object:  StoredProcedure [dbo].[SS_GestionPortafolios_ListarDetalleSolicitudComisiones]    Script Date: 02-09-2020 7:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cristian Muñoz
-- Create date: 02-09-2020
-- Description:	Realiza el calculo de comisiones en los acuerdos comerciales
-- =============================================
CREATE PROCEDURE sp_AcuerdoComercial_CalculoComision
@fondos as xml
AS
BEGIN
SET NOCOUNT ON;
set dateformat dmy;

 BEGIN TRY
    BEGIN TRANSACTION 
 SET NOCOUNT ON ;
-- delete from tbComisionDiaria



-- se eliminan desde la tbComisionDiaria todos los registros de un fondo con uns determinada fecha
delete tbComisionDiaria
from tbComisionDiaria CD    
		inner join
	tbAcuerdo A on CD.IdAcuerdo = A.Id and A.FechaVigenciaHasta is null 
	inner join 
	@fondos.nodes('/Fondos/Fondo') AS T(Item) 
	on  T.Item.value('@Codigo', 'varchar(50)') = A.CODIGO_PRODUCTO  
where Fecha =  T.Item.value('@Fecha', 'varchar(10)')                


---- se insertan los calculos realizados
insert into tbComisionDiaria ( IdAcuerdo, PatrimonioFondo, Comision, FechaHoraCalculo, Fecha)

 SELECT
   A.Id as IdAcuerdo,
   T.Item.value('@Patrimonio', 'decimal(18, 4) ') as Patrimonio,
   A.RebateAnual *  T.Item.value('@Patrimonio', 'decimal(18, 4) ') * 365 AS Comision,
   getdate() as FechaHoraCalculo ,
   T.Item.value('@Fecha', 'varchar(10)') 
 FROM 
   @fondos.nodes('/Fondos/Fondo') AS T(Item)  
   inner join tbAcuerdo A on  T.Item.value('@Codigo', 'varchar(50)') = A.CODIGO_PRODUCTO and A.FechaVigenciaHasta is null
where 
	A.RebateAnual is not null 

    COMMIT
END TRY
BEGIN CATCH

    IF @@TRANCOUNT > 0
        ROLLBACK
END CATCH


END
