GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cristian Muñoz
-- Create date: 02-09-2020
-- Description:	Realiza el calculo de comisiones en los acuerdos comerciales
-- =============================================
CREATE PROCEDURE sp_AcuerdoComercial_GetPatrimonio
@fondos as xml,
@fechaDesde as varchar(10),
@fechaHasta as varchar(10)
AS
BEGIN
SET NOCOUNT ON;
SET DATEFORMAT dmy;

SELECT 
SUM(patrimonio) OVER(PARTITION BY instrumento, fecha) as Patrimonio
, instrumento as  CODIGO_PRODUCTO
, fecha 
from PATRIMONIO_PRODUCTO_DIARIO WITH(NOLOCK)
inner join  @fondos.nodes('/Fondos/Fondo') AS T(Item)  on T.Item.value('@Codigo', 'varchar(50)') = instrumento
where  
CONVERT (datetime,convert(char(8),fecha)) between  @fechaDesde and @fechaHasta  
UNION
SELECT 
SUM(ValorizacionDiaria) OVER(PARTITION BY CODIGOINDENTIFICADORTITULO, FECHAINFO) as Patrimonio
,CODIGOINDENTIFICADORTITULO as CODIGO_PRODUCTO 
,cast( REPLACE(FECHAINFO,'-','') as int) as fecha
from tCC_Custodia_Diario_Prsh  WITH(NOLOCK)
inner join  @fondos.nodes('/Fondos/Fondo') AS T(Item)  on T.Item.value('@Codigo', 'varchar(50)') = CODIGOINDENTIFICADORTITULO
where 
CONVERT (datetime,replace( FECHAINFO,'-','')) between @fechaDesde and @fechaHasta


END
