﻿using System.Web;
using System.Web.Optimization;

namespace LV_Template_MVC4
{
    public class BundleConfig
    {
        // Para obtener más información acerca de Bundling, consulte http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
     
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //           "~/Scripts/jquery/jquery.js"
            //        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                   "~/Scripts/bootstrap.bundle.js",
                      "~/Scripts/moment.js",
                      "~/Scripts/Chart.js",
                      "~/Scripts/sparkline.js",
                      "~/Scripts/sparkline.js",
                      "~/Scripts/jquery.vmap.js",
                      "~/Scripts/jquery.knob.min.js",
                      "~/Scripts/moment.js",
                      "~/Scripts/summernote-bs4.js",
                      "~/Scripts/jquery.overlayScrollbars.js",
                      "~/Scripts/adminlte.js",
                      "~/Scripts/select2.full.js",
                      "~/Scripts/jquery.dataTables.js",
                      "~/Scripts/dataTables.bootstrap4.js",
                      "~/Scripts/iziToast.js",
                       "~/Scripts/jquery.rut.chileno.js",
                       "~/Scripts/jquery.datetimepicker.js",
                       "~/Scripts/jquery.blockUI.js",
                       "~/Scripts/jquery-ui-1.12.1.js",
                       "~/Scripts/dataTables.select.js",
                       "~/Scripts/jquery.validate.js",
                       "~/Scripts/iziModal.js",
                       "~/Scripts/customs.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/adminlte.css",
                      "~/Content/select2-bootstrap4.css",
                      "~/Content/fontawesome-free/css/all.css",
                      "~/Content/icheck-bootstrap.css",
                      "~/Content/jqvmap.css",
                      "~/Content/OverlayScrollbars.css",
                      "~/Content/daterangepicker.css",
                      "~/Content/summernote-bs4.css",
                      "~/Content/select2.css",
                      "~/Content/dataTables.bootstrap4.css",
                      "~/Content/iziToast.css",
                      "~/Content/themes/base/jquery-ui.css",
                      "~/Content/animate.css",
                      "~/Content/select.dataTables.css",
                      "~/Content/iziModal.css",
                      "~/Content/icheck-bootstrap.css"));
        }
    }
}