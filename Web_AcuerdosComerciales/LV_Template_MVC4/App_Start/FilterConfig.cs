﻿using LV_Template_MVC4.Filters;
using System.Web;
using System.Web.Mvc;

namespace LV_Template_MVC4
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new SessionFilterAttribute());
        }
    }
}