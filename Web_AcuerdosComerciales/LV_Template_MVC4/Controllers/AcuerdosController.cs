﻿using LV_Template_MVC4.Interfaces;
using LV_Template_MVC4.Models;
using LV_Template_MVC4.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LV_Template_MVC4.Controllers
{
   
    public class AcuerdosController : Controller
    {

        private readonly IActivosLVServices activosLVServices;
        private readonly IAcuerdosServices acuerdosServices;

        public AcuerdosController()
        {
            activosLVServices = new ActivosLVServices();
            acuerdosServices = new AcuerdosServices();
        }
        //
        // GET: /Acuerdos/

        public ActionResult Index()
        {
            var acuerdos = activosLVServices.Get(2);
            var acuerdoView = new ActivosViewModel()
            {
                AcivoList = acuerdos.ToList(),
                Activo = new Activos(),
                Acuerdo = new Acuerdo()

            };
            return View(acuerdoView);
        }

        [HttpPost]
        public ActionResult Index(ActivosViewModel activoView)
        {
            int idFilter = (int)activoView.SelectFilter;
            var acuerdos = activosLVServices.Get(idFilter);

            activoView.AcivoList = acuerdos.ToList();

            return View("Index", activoView);
        }

        [HttpPost]
        public ActionResult RelacionarView(ActivosViewModel activo)
        {
            // se obtiene el listado seleccionado
            // este listado puede tener o no un acuerdo asignado
            var selectedItem = activo.AcuerdosSelected.Split(',');

            var fondos = activosLVServices.GetByCodes(selectedItem);
            if (!fondos.Any())
            {
                // not found view
                return View("Page404");
            }
            activo.AcivoList = fondos.ToList();
            //si el listado seleccionado presenta valores iguales, dichos valores se establecen como valores por defecto 
            var fondosAsociados = fondos.Where(f => f.IdAcuerdo != null);
            var codigoInterno = fondosAsociados.Select(f => f.CodigoInterno).Distinct().Count() == 1 ? fondos.FirstOrDefault().CodigoInterno : "";
            var fecha = fondosAsociados.Select(f => f.FechaVigenciaDesde).Distinct().Count() == 1 ? fondos.FirstOrDefault().FechaVigenciaDesde : "";
            var tipoPago = fondosAsociados.Select(f => f.TipoPago).Distinct().Count() == 1 ? fondos.FirstOrDefault().TipoPago : "";
            var tipoRebate = fondosAsociados.Select(f => f.TipoRebate).Distinct().Count() == 1 ? fondos.FirstOrDefault().TipoRebate : "";
            var rebate = fondosAsociados.Select(f => f.RebateAnual).Distinct().Count() == 1 ? fondos.FirstOrDefault().RebateAnual : null;
            var manager = fondosAsociados.Select(f => f.IdManager).Distinct().Count() == 1 ? fondos.FirstOrDefault().IdManager : null;
            var managerFree = fondosAsociados.Select(f => f.ManagementFree).Distinct().Count() == 1 ? fondos.FirstOrDefault().ManagementFree : null;
            var acuerdo = fondosAsociados.Select(f => f.AcuerdoComercial).Distinct().Count() == 1 ? fondos.FirstOrDefault().AcuerdoComercial : null;
            DateTime fechaVig;
            activo.Acuerdo = new Acuerdo()
            {
                CodigoInterno = codigoInterno,
                TipoPago = tipoPago,
                TipoRebate = tipoRebate,
                RebateAnual = rebate,
                IdManager = manager ?? 0,
                ManagementFree = managerFree,
                AcuerdoComercial = acuerdo,
                FechaVigenciaDesde = DateTime.TryParse(fecha, out fechaVig) ? fechaVig : (DateTime?)null

            };



            return View(activo);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Relacionar(ActivosViewModel activoView)
        {
            // validaciones de todos los parametros
            if (!ModelState.IsValid)
            {
                List<string> errores = new List<string>();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errores.Add(error.ErrorMessage);
                    }
                }

                return Json(new { data = "NOK", errormesages = errores.ToArray() });
            }


            try
            {
                Acuerdo acuerdo = activoView.Acuerdo;
                if (acuerdo.TipoRebate == "V" && acuerdo.ManagementFree.HasValue && acuerdo.AcuerdoComercial.HasValue)
                {
                    acuerdo.RebateAnual = acuerdo.ManagementFree * acuerdo.AcuerdoComercial;
                }
                // cada uno de los activos seleccionados se transforma en un acuerdo comercial
                AcuerdoList acuerdos = new AcuerdoList();
                acuerdos.Acuerdos = activoView.AcivoList.Select(a => new Acuerdo()
                {
                    CODIGO_PRODUCTO = a.CODIGO_PRODUCTO,
                    AcuerdoComercial = acuerdo.AcuerdoComercial,
                    CodigoInterno = acuerdo.CodigoInterno,
                    FechaVigenciaDesde = acuerdo.FechaVigenciaDesde,
                    IdManager = acuerdo.IdManager,
                    ManagementFree = acuerdo.ManagementFree,
                    NemoBaseComercial = acuerdo.CodigoInterno,
                    RebateAnual = acuerdo.RebateAnual,
                    TipoPago = acuerdo.TipoPago,
                    TipoRebate = acuerdo.TipoRebate,
                    UsuarioReg = User.Identity.Name,

                });

                acuerdosServices.Relacionar(acuerdos);
                return Json(new { data = "OK" });

            }
            catch (Exception ex)
            {
                return Json(new { data = "NOK", errormesages = new List<string> { ex.Message }.ToArray() });
            }
        }


        public ActionResult Edit(int id)
        {
            var acuerdo = acuerdosServices.GetById(id);
            if (null == acuerdo)
            {
                // not found view
                return View("Page404");
            }
            IEnumerable<Acuerdo> acuerdosHistoricos = acuerdosServices.GetHistoricos(acuerdo.CODIGO_PRODUCTO);

            AcuerdoViewModel view = new AcuerdoViewModel()
            {
                Acuerdo = acuerdo,
                Acuerdos = acuerdosHistoricos
            };
            return View(view);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(Acuerdo acuerdo)
        {
            acuerdo.UsuarioReg = User.Identity.Name;
            // validaciones de todos los parametros
            if (!ModelState.IsValid)
            {
                List<string> errores = new List<string>();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errores.Add(error.ErrorMessage);
                    }
                }

                return Json(new { data = "NOK", errormesages = errores.ToArray() });
            }

            if (acuerdo.TipoRebate == "V" && acuerdo.ManagementFree.HasValue && acuerdo.AcuerdoComercial.HasValue)
            {
                acuerdo.RebateAnual = acuerdo.ManagementFree * acuerdo.AcuerdoComercial;
            }

            try
            {
                acuerdosServices.Edit(acuerdo);
                return Json(new { data = "OK" });

            }
            catch (Exception ex)
            {
                return Json(new { data = "NOK", errormesages = new List<string> { ex.Message }.ToArray() });
            }

        }

    }
}
