﻿using LV_Template_MVC4.Interfaces;
using LV_Template_MVC4.Models;
using LV_Template_MVC4.Services;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace LV_Template_MVC4.Controllers
{

    public class ManagersController : Controller
    {
        private readonly IManagerServices managerServices;
        public ManagersController()
        {
            managerServices = new ManagerServices();
        }


        /// <summary>
        /// Obtiene el listado de managers
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var managers = managerServices.Get();

            ManagerViewModel viewModel = new ManagerViewModel()
            {
                Manager = new Manager(),
                ManagerList = managers

            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(Manager manager)
        {

            // validaciones de todos los parametros
            if (!ModelState.IsValid)
            {
                List<string> errores = new List<string>();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errores.Add(error.ErrorMessage);
                    }
                }

                return Json(new { data = "NOK", errormesages = errores.ToArray() });
            }

            try
            {
                managerServices.Add(manager);
                return Json(new { data = "OK" });

            }
            catch (Exception ex)
            {
                return Json(new { data = "NOK", errormesages = new List<string> { ex.Message }.ToArray() });
            }
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            List<string> errores = new List<string>();
            try
            {
                managerServices.Delete(id);
                return Json(new { data = "OK" });
            }
            catch (Exception ex)
            {
                errores.Add(ex.Message);
                return Json(new { data = "NOK", errormesages = errores.ToArray() });
            }

        }


        public ActionResult Edit(int id)
        {
            var managers = managerServices.GetById(id);

            if (null == managers)
            {
                // not found view
                return View("Page404");
            }

            return View(managers);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(Manager manager)
        {

            // validaciones de todos los parametros
            if (!ModelState.IsValid)
            {
                List<string> errores = new List<string>();
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errores.Add(error.ErrorMessage);
                    }
                }

                return Json(new { data = "NOK", errormesages = errores.ToArray() });
            }

            try
            {
                managerServices.Update(manager);
                return Json(new { data = "OK" });

            }
            catch (Exception ex)
            {
                return Json(new { data = "NOK", errormesages = new List<string> { ex.Message }.ToArray() });
            }
        }

    }
}
