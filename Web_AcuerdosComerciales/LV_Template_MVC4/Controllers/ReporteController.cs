﻿using LV_Template_MVC4.Interfaces;
using LV_Template_MVC4.Models;
using LV_Template_MVC4.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LV_Template_MVC4.Controllers
{
   
    public class ReporteController : Controller
    {
        private readonly IReportServices reportServices;
        public ReporteController()
        {
            reportServices = new ReportServices();
        }

        public ActionResult Diario()
        {
            ReporteViewModel view = new ReporteViewModel() {
                ReporteDiarioList = new List<ReporteDiario>()
            };

            return View(view);
        }

        [HttpPost]
        public ActionResult Diario(ReporteViewModel view)
        {
            view.ReporteDiarioList = reportServices.GetDiario(view.PeriodoSeleccionado.ToString(), view.IdManager, view.Fondo);

            return View("Diario", view);
        }
        public ActionResult Resumen()
        {
            ReporteViewModel view = new ReporteViewModel()
            {
               ReporteResumenList = new List<ReporteResumen>()
            };

            return View(view);
        }

        [HttpPost]
        public ActionResult Resumen(ReporteViewModel view)
        {
            view.ReporteResumenList = reportServices.GetResumen(view.PeriodoSeleccionado.ToString());

            return View("Resumen", view);
        }



    }
}
