﻿using LV_Template_MVC4.Controllers;
using LV_Template_MVC4.Models;
using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml;

namespace LV_Template_MVC4.Filters
{
    public class SessionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                base.OnActionExecuting(filterContext);
                if (ConfigurationManager.AppSettings["IsDev"] == "true")
                    return;
                Usuario user = (Usuario)HttpContext.Current.Session["user"];
                if (user == null && !(filterContext.Controller is AccountController))
                {
                    // get info perfilamiento
                    var usuario = HttpContext.Current.User.Identity.Name.Split('\\')[1];

                    WSLogin.WSLoginEtradingSoapClient oCliente = new WSLogin.WSLoginEtradingSoapClient();
                    string tmpXML = "<WSParam>";
                    tmpXML += "<Parametros>";
                    tmpXML += "<Content>";
                    tmpXML += "<xml>";
                    tmpXML += "<user>" + usuario.ToLower() + "</user>";
                    tmpXML += "<aplication>Acuerdos Comerciales</aplication>";
                    tmpXML += "</xml>";
                    tmpXML += "</Content>";
                    tmpXML += "</Parametros>";
                    tmpXML += "</WSParam>";
                    var xmlLogin = oCliente.Met_UserProfileX(tmpXML);

                    XmlDocument xml = new XmlDocument();
                    xml.LoadXml(xmlLogin);

                    XmlNodeList nodes = xml.SelectNodes("WS/Resultado/xml");
                    Usuario usuarioP = null;
                    foreach (XmlNode item in nodes)
                    {

                        var estado = Convert.ToBoolean(item["estado"].InnerText);
                        if (estado)
                        {
                            usuarioP = new Usuario()
                            {
                                aplicacion = item["aplicacion"].InnerText,
                                estado = estado,
                                idusuario = item["idusuario"].InnerText,
                                usuario = item["usuario"].InnerText,
                                perfil = item["perfil"].InnerText

                            };

                            HttpContext.Current.Session["user"] = usuarioP;

                        }
                    }

                    // AccountController == false para evitar multiples redirect
                    if (null == usuarioP && !(filterContext.Controller is AccountController))
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                        {
                            controller = "Account",
                            action = "Index"
                        }));
                    }
                }
            }
            catch (Exception)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "Index"
                }));
            }
        }
    }
}