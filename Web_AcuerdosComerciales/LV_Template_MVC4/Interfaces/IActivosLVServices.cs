﻿using LV_Template_MVC4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LV_Template_MVC4.Interfaces
{
    public interface IActivosLVServices
    {
        IEnumerable<Activos> Get(int id);
        IEnumerable<Activos> GetByCodes(IEnumerable<string> codigos);
    }


}