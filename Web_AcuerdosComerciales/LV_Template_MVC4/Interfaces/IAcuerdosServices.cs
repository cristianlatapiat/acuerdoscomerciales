﻿using LV_Template_MVC4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LV_Template_MVC4.Interfaces
{
    public interface IAcuerdosServices
    {
        IEnumerable<Acuerdo> Get(string id = "");
        void Relacionar(AcuerdoList acuerdos);
        Acuerdo GetById(int id);
        IEnumerable<Acuerdo> GetHistoricos(string codigoProducto);
        void Edit(Acuerdo acuerdo);
    }
}