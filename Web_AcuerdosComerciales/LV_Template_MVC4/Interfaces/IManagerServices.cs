﻿using LV_Template_MVC4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV_Template_MVC4.Interfaces
{
    public interface IManagerServices
    {
        IEnumerable<Manager> Get();
        Manager Add(Manager manager);
        void Delete(int id);
        Manager GetById(int id);

        void Update(Manager manager);
    }
}
