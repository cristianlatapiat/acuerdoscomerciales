﻿using LV_Template_MVC4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LV_Template_MVC4.Interfaces
{
    public interface IReportServices
    {
        IEnumerable<int> GetPeriodos();
        IEnumerable<ReporteDiario> GetDiario(string periodo, int idManager, string fondo);
        IEnumerable<ReporteResumen> GetResumen(string periodo);
        
    }
}