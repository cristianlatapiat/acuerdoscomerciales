﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc.Html;

namespace LV_Template_MVC4.Models
{
    public class Activos
    {
        public int? IdAcuerdo { get; set; }
        [Display(Name = "Cod. Producto")]
        public string CODIGO_PRODUCTO { get; set; }

        [Display(Name = "Cod. Interno")]
        public string ISIN { get; set; }

        [Display(Name = "Manager")]
        public string NombreManager { get; set; }

        [Display(Name = "Nombre Activo")]
        public string NOMBRE_ACTIVO { get; set; }

        [Display(Name = "Tipo Producto")]
        public string TipoProducto { get; set; }

        [Display(Name = "Categoria")]
        public string CAT_NOMBRE { get; set; }
        [Display(Name = "Tipo Pago")]
        public string TipoPago { get; set; }
        [Display(Name = "Tipo Rebate")]
        public string TipoRebate { get; set; }
        [Display(Name = "Management Free")]
        public decimal? ManagementFree { get; set; }
        [Display(Name = "Rebate")]
        public decimal? RebateAnual { get; set; }
        [Display(Name = "Acuerdo")]
        public decimal? AcuerdoComercial { get; set; }
        [Display(Name = "Vigencia Desde")]
        public string FechaVigenciaDesde { get; set; }
        public int? IdManager { get; set; }
        [Display(Name = "Codigo Interno")]
        public string CodigoInterno { get; set; }
        public string DescipcionTipoPago { get; set; }
        public string DescipcionTipoRebate { get; set; }




    }

    public enum SelectFilter
    {
        Todos,
        SinAsignar,
        Asignados,
        Activos,
        Inactivos,
        EnPortafolio
    }
}