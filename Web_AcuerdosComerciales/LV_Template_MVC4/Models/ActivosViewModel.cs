﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LV_Template_MVC4.Models
{
    public class ActivosViewModel
    {
        public IList<Activos> AcivoList{ get; set; }
        public Activos Activo { get; set; }
        public SelectFilter SelectFilter { get; set; }

        public string AcuerdosSelected { get; set; }

        public Acuerdo Acuerdo { get; set; }


    }
}