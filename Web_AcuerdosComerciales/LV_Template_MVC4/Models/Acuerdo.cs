﻿using LV_Template_MVC4.Interfaces;
using LV_Template_MVC4.Models;
using LV_Template_MVC4.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LV_Template_MVC4.Models
{
    public class Acuerdo
    {
        public int Id { get; set; }
        [Display(Name = "Manager")]
        public int IdManager { get; set; }
        [Display(Name = "Fondo")]
        public string CODIGO_PRODUCTO { get; set; }
        [Display(Name = "Fecha Inicio")]
        public DateTime? FechaVigenciaDesde { get; set; }
        [Display(Name = "Rebate")]
        [Range(0, 100, ErrorMessage = "Favor ingrese porcentaje de Rebate válido")]
        public decimal? RebateAnual { get; set; }
        [Display(Name = "Tipo Pago")]
        public string TipoPago { get; set; } = "";
        [Display(Name = "Tipo Rebate")]
        public string TipoRebate { get; set; } = "";
        [Display(Name = "Management Free")]
        [Range(0, 100, ErrorMessage = "Favor ingrese porcentaje de ManagementFree válido")]
        public decimal? ManagementFree { get; set; }
        [Display(Name = "Acuerdo")]
        [Range(0, 100, ErrorMessage = "Favor ingrese porcentaje de Acuerdo válido")]
        public decimal? AcuerdoComercial { get; set; }
        public DateTime? FechaHoraCambio { get; set; }
        [Display(Name = "Usuario")]
        public string UsuarioReg { get; set; }
        public string NemoBaseComercial { get; set; }

        [Display(Name = "Codigo Interno")]
        public string CodigoInterno { get; set; }
        public string IdDocumentos { get; set; }
        [Display(Name = "Vigente Hasta")]
        public DateTime? FechaVigenciaHasta { get; set; }

        public IEnumerable<SelectListItem> ManagerList { get; private set; }
        public string Manager { get; set; }
        [Display(Name = "Tipo Pago")]
        public string DescipcionPago { get; set; }
        [Display(Name = "Tipo Rebate")]
        public string DescripcionRebate { get; set; }
        public IEnumerable<SelectListItem> TiposPagos { get; private set; }
        public IEnumerable<SelectListItem> TiposRebates { get; private set; }
        public Acuerdo()
        {
            LoadManagers();
            LoadTiposDePago();
            LoadTiposDeRebates();

        }

        private void LoadManagers()
        {
            var managers = new ManagerServices().Get();
            ManagerList = managers.Select(m => new SelectListItem()
            {

                Text = m.Nombre,
                Value = m.Id.ToString()
            });
        }

        private void LoadTiposDePago()
        {
            TiposPagos = new List<SelectListItem>() {
                new SelectListItem() {
                    Text = "Mensual",
                    Value = "M"

                },
                 new SelectListItem() {
                    Text = "Trimestral",
                    Value = "T"

                },
                  new SelectListItem() {
                    Text = "Anual",
                    Value = "A"
                }
            };
        }

        private void LoadTiposDeRebates()
        {
            TiposRebates = new List<SelectListItem>() {
                new SelectListItem() {
                    Text = "Fijo",
                    Value = "F"

                },
                 new SelectListItem() {
                    Text = "Variable",
                    Value = "V"

                }
            };
        }
    }


}