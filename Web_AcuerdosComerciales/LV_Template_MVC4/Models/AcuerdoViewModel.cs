﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LV_Template_MVC4.Models
{
    public class AcuerdoViewModel
    {
        public IEnumerable<Acuerdo> Acuerdos { get; set; }
        public Acuerdo Acuerdo { get; set; }

    }
}