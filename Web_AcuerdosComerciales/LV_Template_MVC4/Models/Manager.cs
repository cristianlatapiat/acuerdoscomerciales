﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LV_Template_MVC4.Models
{
    public class Manager
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(250)]
        [Display(Name = "Manager")]
        public string Nombre { get; set; }
        [Required]
        [MaxLength(1)]
        public string Plataforma { get; set; }
        [MaxLength(250)]
        public string Contacto { get; set; }
        [MaxLength(250)]
        [DataType(DataType.EmailAddress)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail es inválido")]
        public string Email { get; set; }

        [MaxLength(50)]
        public string Telefono { get; set; }

        [Display(Name = "Fecha Contrato")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public string FechaContrato { get; set; }
        public string DocumentosGuid { get; set; }
        [Display(Name = "")]
        public HttpPostedFileBase Archivo { get; set; }

        public IEnumerable<SelectListItem> PlataformaList { get; private set; }
        public string TieneAcuerdos { get; set; }
        public string PlataformaName { get; set; }
        public string UrlDocumentos
        {
            get
            {
                return ConfigurationManager.AppSettings["UrlDocumentos"].ToString();

            }
        }


        public Manager()
        {
            var listado = new List<SelectListItem>();
            listado.Add(new SelectListItem
            {
                Text = "Internacional",
                Value = "I"
            });
            listado.Add(new SelectListItem
            {
                Text = "Local",
                Value = "L"
            });
            PlataformaList = listado;

        }
    }
}