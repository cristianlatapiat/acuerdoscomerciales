﻿using System.Collections.Generic;
using System.Web;

namespace LV_Template_MVC4.Models
{
    public class ManagerViewModel
    {
        public IEnumerable<Manager> ManagerList { get; set; }
        public Manager Manager { get; set; }

        public HttpPostedFileBase UploadedFile { get; set; }
    }
}