﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LV_Template_MVC4.Models
{
    public class ReporteDiario
    {
        public string Fondo { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal? ManagementFree { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal? RebateAnual { get; set; }
        public string Fecha { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal? TotalManagementFree { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal? TotalRebateAnual { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal? ManagementFreeDiario { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal? RebateDiario { get; set; }
        public int IdAcuerdo { get; set; }
        public decimal PatrimonioFondo { get; set; }
        public decimal Comision { get; set; }
        public string NombreManager { get; set; }
        public string Periodo { get; set; }
    }
}