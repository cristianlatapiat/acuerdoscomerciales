﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LV_Template_MVC4.Models
{
    public class ReporteResumen
    {
        public string NombreManager { get; set; }
        public string Fondo { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal? ManagementFree { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal? RebateAnual { get; set; }
        public string TipoRebate { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal? TotalManagementFree { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal? TotalRebateAnual { get; set; }
        public decimal? SumaTotalManagementFree { get; set; }
        public decimal? SumaTotalRebateAnual { get; set; }
        public string Periodo { get; set; }
    }
}