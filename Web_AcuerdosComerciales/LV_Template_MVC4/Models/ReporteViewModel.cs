﻿using LV_Template_MVC4.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LV_Template_MVC4.Models
{
    public class ReporteViewModel
    {
   
        public string Fondo { get; set; }
        public IEnumerable<SelectListItem> ManagerList { get; private set; }
        public IEnumerable<SelectListItem> Periodos { get; private set; }
        public int PeriodoSeleccionado { get; set; }
        public int IdManager { get; set; }
        public IEnumerable<ReporteDiario> ReporteDiarioList { get; set; }
        public IEnumerable<ReporteResumen> ReporteResumenList { get; set; }
        


        public ReporteViewModel()
        {
            LoadManager();
            LoadPeriodos();
        }

        private void LoadPeriodos()
        {
            var periodos = new ReportServices().GetPeriodos();

            Periodos = periodos.Select(p => new SelectListItem()
            {
                Text = p.ToString(),
                Value = p.ToString()
            });
        }

        private void LoadManager()
        {
            var managers = new ManagerServices().Get();
            ManagerList = managers.Select(m => new SelectListItem()
            {

                Text = m.Nombre,
                Value = m.Id.ToString()
            });
        }
    }
}