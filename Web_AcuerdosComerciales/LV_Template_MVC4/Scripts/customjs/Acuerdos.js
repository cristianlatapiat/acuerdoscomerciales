﻿$(document).ready(function () {
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });


    // buscar por el filtro seleccionado
    $("[name='SelectFilter']").click(function () {
        var radioValue = $("[name='SelectFilter']:checked").val();

        document.getElementById("formBuscar").submit();

        //se recarga la grilla con la informacion
        $('#gridActivos').on('draw.dt', function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

    });
    var link = "";
    // se agregan los input de busqueda
    $('#gridActivos thead tr').clone(true).appendTo('#gridActivos thead');
    $('#gridActivos thead tr:eq(0) th').each(function (i) {
        var title = $(this).text().trim();
        if (i == 0) {
            var html = '<button class="btn btn-outline-info js-relacionar alinear-centro-medio" id="btnRelacionar" data-toggle="tooltip" data-placement="top" title="Relacionar/Actualizar"><i class="fas fa-user-plus"></i>&nbsp;Relacionar</button>';
            $(this).html(html);

        }
        else if (title != "" && i > 1) {
            var htl = `<input style="width: 130px;" type="text" placeholder="Buscar ${title}" />`;
            $(this).html(htl);
            title = title
            link = link + `<a href="#" class="toggle-vis" data-column="${i}">${title}</a> -`;

            $('input', this).keyup(function () {
                if (tabla.column(i).search() !== this.value) {
                    tabla.column(i)
                        .search(this.value)
                        .draw();
                }
            });

        }

    });
  

    // se agregan todos los link 
    $('#linkColumn').html(link.slice(0, -1));

    $('a.toggle-vis').click(function (e) {
        e.preventDefault();

        // Get the column API object
        var column = tabla.column($(this).attr('data-column'));

        // Toggle the visibility
        column.visible(!column.visible());
        setLink();

    });
  
    // se agrega la funcionalidad de datatable
    var tabla = $("#gridActivos").DataTable({
        lengthMenu: [[20, 40, 60], [20, 40, 60]],
        order: [],
        pageLength: 20,
        language: {
            url: "./Scripts/Spanish.json",
            select: {
                rows: "%d filas seleccionadas"
            }
        },
        columnDefs: [
           {
               orderable: false,
               className: 'select-checkbox',
               targets: 0
           },
            { targets: 1, visible: false },
            { targets: 7, visible: false },
            { targets: 10, visible: false },
        ],
        select: {
            'style': 'multi'
        },
        //para usar los botones exportar
        responsive: "true",
        dom: 'Bfrtilp',
        buttons: [
			{
			    extend: 'excelHtml5',
			    text: '<i class="fas fa-file-excel"></i> Excel',
			    titleAttr: 'Exportar a Excel',
			    className: 'btn btn-outline-success',
			    init: function (api, node, config) {
			        $(node).removeClass('dt-button')
			    }
			}
        ],
        drawCallback: function () {
            $('[data-toggle="tooltip"]').tooltip();
        }
    });

    function setLink() {
        var columns = tabla.columns().visible().toArray();

        for (var i = 0; i < columns.length; i++) {
            if (!columns[i]) {
                $(`[data-column=${i}]`).addClass("blue");
            }
            else {
                $(`[data-column=${i}]`).removeClass("blue");
            }
        }
    }

    setLink();

    //btnRelacionar 
    $("#btnRelacionar").click(function () {

        var data = tabla.rows({ selected: true }).data().toArray();
        if (data.length == 0) {
            iziToast.error({
                title: 'Relacion Managers-Fondo',
                message: "debe seleccionar al menos un fondo",
                overlay: true,
                displayMode: 'once',
                position: 'center',
                progressBarColor: 'rgb(0, 255, 184)',
                timeout: 5000,
                buttons: [
                    ['<button>Ok</button>', function (instance, toast) {
                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                    }, true]
                ]
            });
        }
        else {
            // estableciendo los controles del modal
            var codigos = new Array()
            for (var i = 0; i < data.length; i++) {
                codigos.push(data[i][3]);
            }
            $("#acuerdosSeleccionados").val(codigos.join(','))
            document.getElementById("formRelacionar").submit();
        }




    });

    //
    // se agrega la funcionalidad de datatable
    var tablaAcuerdos = $("#gridAcuerdos").DataTable({
        lengthMenu: [[20, 40, 60], [20, 40, 60]],
        order: [],
        pageLength: 20,
        language: {
            url: "./Scripts/Spanish.json",
        },
        columnDefs: [],
        drawCallback: function () {
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
});