﻿$(document).ready(function () {
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    var fecha = $('#txtFecha');
    if (fecha.val() != "") {
        let f = fecha.val().substring(0, 10);
        fecha.val(f);
    }


    // se agrega la funcionalidad de datatable
    var tabla = $("#gridAcuerdosHistoricos").DataTable({
        lengthMenu: [[20, 40, 60], [20, 40, 60]],
        order: [],
        pageLength: 20,
        language: {
            url: "../../Scripts/Spanish.json"
        },
        columnDefs: [
            { targets: 1, visible: false },
            { targets: 2, visible: false },
            { targets: 3, visible: false },
            { targets: 9, visible: false },
        ],
        //para usar los botones exportar
        responsive: "true",
        dom: 'Bfrtilp',
        buttons: [
			{
			    extend: 'excelHtml5',
			    text: '<i class="fas fa-file-excel"></i> Excel',
			    titleAttr: 'Exportar a Excel',
			    className: 'btn btn-outline-success',
			    init: function (api, node, config) {
			        $(node).removeClass('dt-button')
			    }
			}
        ],
        drawCallback: function () {
            $('[data-toggle="tooltip"]').tooltip();
        }
    });

  
    // se agregan todos los link 
    var link = "";
    $('#gridAcuerdosHistoricos thead tr:eq(0) th').each(function (i) {
        var title = $(this).text().trim();
        if (title != "") {
            link = link + `<a href="#" class="toggle-vis" data-column="${i}">${title}</a> -`;

        }
    });

    $('#linkColumn').html(link.slice(0, -1));

    $('a.toggle-vis').click(function (e) {
        e.preventDefault();

        // Get the column API object
        var column = tabla.column($(this).attr('data-column'));

        // Toggle the visibility
        column.visible(!column.visible());
        setLink();
    });

    function setLink() {
        var columns = tabla.columns().visible().toArray();

        for (var i = 0; i < columns.length; i++) {
            if (!columns[i]) {
                $(`[data-column=${i}]`).addClass("blue");
            }
            else {
                $(`[data-column=${i}]`).removeClass("blue");
            }
        }
    }

    setLink();

    var isAcuerdo = $(".isAcuerdo");
    var isRebate = $(".isRebate");
    $("#ddlTiposRebates").change(function () {
        var rebate = $(this).val();

        if (rebate == "V") {
            isRebate.hide();
            isAcuerdo.show();
            $("#txtRebate").val("");
        }
        else if (rebate == "F") {
            isRebate.show();
            isAcuerdo.hide();
            $("#txtAcuerdo").val("");

        }
        else {
            isRebate.hide();
            $("#txtRebate").val("");

            isAcuerdo.hide();
            $("#txtAcuerdo").val("");
        }
    });


    $("#ddlTiposRebates").trigger("change");

    //Limpiar mensaje de error
    $("#txtFecha").focus(function () {
        $("#txtFecha").css('border', '1px solid #ccc');
        document.getElementById('mensajeFecha').style.display = 'none';
    });
    $("#ddlManagers").change(function () {
        $("#ddlManagers").css('border', '1px solid #ccc');
        document.getElementById('mensajeManagers').style.display = 'none';
    });
    $("#btnActualizar").click(function () {
        var ingresar = true;
        var manager = $("#ddlManagers").val();
        if (manager == "") {
            document.getElementById('mensajeManagers').style.display = 'block';
            $("#ddlManagers").css('border', '1px solid #b94a48');
            ingresar = false;
        }
        if (fecha.val() == "") {
            document.getElementById('mensajeFecha').style.display = 'block';
            fecha.css('border', '1px solid #b94a48');
            ingresar = false;
        }

        if (ingresar) {

            $.ajax({
                type: "POST",
                url: "/Acuerdos/Edit",
                data: $("#formEditAcuerdo").serialize(),
                success: function (data) {

                    if (data.data === "OK") {

                        iziToast.success({
                            icon: 'far fa-check-circle',
                            title: 'Mantenedor Acuerdos',
                            message: 'Registro ingresado correctamente!',
                            position: 'center',
                            overlay: true,
                            displayMode: 'once',
                            position: 'center',
                            progressBarColor: 'rgb(0, 255, 184)',
                            timeout: 5000,
                            buttons: [
                                ['<button>Ok</button>', function (instance, toast) {

                                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                    window.location.href = "/Acuerdos/Index";

                                }, true],

                            ]
                        });

                    } else {
                        var errores = data.errormesages != null ? data.errormesages.join('\n') : ' Error! No Se pudo relizar la operación'
                        iziToast.error({
                            title: 'Mantenedor Acuerdos',
                            message: errores,
                            overlay: true,
                            displayMode: 'once',
                            position: 'center',
                            progressBarColor: 'rgb(0, 255, 184)',
                            timeout: 5000,
                            buttons: [
                                ['<button>Ok</button>', function (instance, toast) {
                                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                }, true],
                            ]
                        });
                    }
                }
            });
        }

    });

});


