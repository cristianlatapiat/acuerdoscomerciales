﻿// FORMAT FIELD (keypress)
// Permite sólo ingresar números, coma (decimal), signo negativo y borrar.
// id = Atributo 'Id' del input (text).
function FormatFieldDown(id, isDecimal = true) {
    field = document.getElementById(id);

    if (Number.isInteger(parseInt(event.key)) ||
        event.key == "-" ||
        event.key == "," ||
        event.key == "Tab" ||
        event.key == "Backspace" ||
        event.key == "Delete" ||
        event.key == "ArrowLeft" ||
        event.key == "ArrowRight" ||
        event.key == "ArrowDown" ||
        event.key == "ArrowUp") {
        if (event.key == "," && isDecimal == false || event.key == "," && field.value.indexOf(",") >= 0) {
            event.preventDefault();
        }

        if (event.key == "-" && field.value.indexOf("-") >= 0) {
            event.preventDefault();
        }
    }
    else {
        event.preventDefault();
    }
};