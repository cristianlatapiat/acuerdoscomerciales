﻿$(document).ready(function () {

    var fecha = $('#txtFecha');
    if (fecha.val() != "") {
        let f = fecha.val().substring(0, 10);
        fecha.val(f);
    }

    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
    var link = "";
    $('#gridManagers thead tr').clone(true).appendTo('#gridManagers thead');
    $('#gridManagers thead tr:eq(0) th').each(function (i) {
        var title = $(this).text().trim();
        if (title != "" && i > 3) {
            var htl = `<input style="width: 130px;" type="text" placeholder="Buscar ${title}" />`;
            $(this).html(htl);
            title = title
            link = link + `<a href="#" class="toggle-vis" data-column="${i}">${title}</a> -`;

            $('input', this).keyup(function () {
                if (tabla.column(i).search() !== this.value) {
                    tabla.column(i)
                        .search(this.value)
                        .draw();
                }
            });

        }

    });

    // se agregan todos los link 
    $('#linkColumn').html(link.slice(0, -1));

    $('a.toggle-vis').click(function (e) {
        e.preventDefault();
       
        // Get the column API object
        var column = tabla.column($(this).attr('data-column'));

        // Toggle the visibility
        column.visible(!column.visible());

        var columns = tabla.columns().visible().toArray();

        for (var i = 0; i < columns.length; i++) {
            if (!columns[i]) {
                $(`[data-column=${i}]`).addClass("blue")
            }
            else {
                $(`[data-column=${i}]`).removeClass("blue");
            }
        }
    });

    // se agrega la funcionalidad de datatable
    var tabla = $("#gridManagers").DataTable({
        lengthMenu: [[20, 40, 60], [20, 40, 60]],
        pageLength: 20,
        language: {
            url: "./Scripts/Spanish.json"
        },
        columnDefs: [
             { targets: 0, visible: false },
             { targets: 1, visible: false },
             { targets: 2, visible: false },
             { targets: 3, visible: false }
        ],
        //para usar los botones exportar
        responsive: "true",
        dom: 'Bfrtilp',
        buttons: [
			{
			    extend: 'excelHtml5',
			    text: '<i class="fas fa-file-excel"></i> Excel',
			    titleAttr: 'Exportar a Excel',
			    className: 'btn btn-outline-success',
			    init: function (api, node, config) {
			        $(node).removeClass('dt-button')
			    },
			    exportOptions: {
			        columns: [1, 4, 5, 6, 7, 8, 9]
			    }
			}
        ],
        drawCallback: function () {
            $('[data-toggle="tooltip"]').tooltip();
        }
    });


    //Limpiar mensaje de error
    $("#txtNombre").keyup(function () {
        $("#txtNombre").css('border', '1px solid #ccc');
        document.getElementById('mensajeNombre').style.display = 'none';
    });

    $("#ddlPlataforma").change(function () {
        $("#ddlPlataforma").css('border', '1px solid #ccc');
        document.getElementById('mensajePlataforma').style.display = 'none';
    });

    function ValidaManager() {
        var ingresar = true;

        var nombre = $("#txtNombre").val();
        var plataforma = $("#ddlPlataforma").val();

        if (nombre === "") {
            document.getElementById('mensajeNombre').style.display = 'block';
            $("#txtNombre").css('border', '1px solid #b94a48');
            ingresar = false;
        }

        if (plataforma === "") {
            document.getElementById('mensajePlataforma').style.display = 'block';
            $("#ddlPlataforma").css('border', '1px solid #b94a48');
            ingresar = false;
        }

        return ingresar;
    }
    function CallPostApi(url, form) {
        $.ajax({
            type: "POST",
            url: url,
            data: form,
            success: function (data) {

                if (data.data === "OK") {

                    iziToast.success({
                        icon: 'far fa-check-circle',
                        title: 'Mantenedor Managers',
                        message: 'Registro ingresado correctamente!',
                        position: 'center',
                        overlay: true,
                        displayMode: 'once',
                        position: 'center',
                        progressBarColor: 'rgb(0, 255, 184)',
                        timeout: 5000,
                        buttons: [
                            ['<button>Ok</button>', function (instance, toast) {

                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                window.location.href = "/Managers";

                            }, true],

                        ]
                    });

                } else {
                    var errores = data.errormesages != null ? data.errormesages.join('\n') : ' Error! No Se pudo relizar la operación'
                    iziToast.error({
                        title: 'Mantenedor Managers',
                        message: errores,
                        overlay: true,
                        displayMode: 'once',
                        position: 'center',
                        progressBarColor: 'rgb(0, 255, 184)',
                        timeout: 5000,
                        buttons: [
                            ['<button>Ok</button>', function (instance, toast) {
                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            }, true],
                        ]
                    });
                }
            }
        });
    }
    //actualiza un managers
    $("#btnActualizar").click(function () {
        var ingresar = ValidaManager();

        if (ingresar) {
            CallPostApi("/Managers/Edit", $("#formEditManagers").serialize())
        }


    });
    //Ingresar alumno
    $("#btnIngresar").click(function () {

        var ingresar = ValidaManager();

        if (ingresar) {

            CallPostApi("/Managers/Create", $("#formIngresoManagers").serialize())
        }

    });

    $('#gridManagers').on('click', '.js-Cargar', function () {
        var button = $(this);
        var data = tabla.row($(this).closest('tr')).data();
        var guid = data[2];
        var url = data[3];
        document.getElementById('cargaArchivos').src = url + guid;


        $('#modalCargarArchivos').modal('show');
    });

    $('#gridManagers').on('click', '.js-Eliminar', function () {
        var button = $(this);
        var data = tabla.row($(this).closest('tr')).data();
        var id = data[0];
        var tieneAcuerdos = data[1];
        var mensaje = tieneAcuerdos == "SI" ? "El manager tiene acuerdos comerciales relacionados. ¿Desea continuar?" : "¿Está seguro de eliminar la fila seleccionada?";

        iziToast.show({
            theme: 'dark',
            icon: 'far fa-question-circle',
            title: tieneAcuerdos == "SI" ? '*** ATENCION ***' : 'Mantenedor Managers',
            message: mensaje,
            overlay: true,
            displayMode: 'once',
            position: 'center',
            progressBarColor: 'rgb(0, 255, 184)',
            timeout: 0,
            buttons: [
                ['<button><b>Si</b></button>', function (instance, toast) {
                    $.ajax({
                        type: "POST",
                        url: "/Managers/Delete",
                        data: {
                            id: id
                        },
                        datatype: "",
                        success: function (data) {
                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                            if (data.data === "OK") {
                                iziToast.success({
                                    icon: 'far fa-check-circle',
                                    title: 'Mantenedor Managers',
                                    message: 'Registro eliminado correctamente!',
                                    position: 'center',
                                    buttons: [
                                        ['<button>Ok</button>', function (instance, toast) {

                                            tabla.row(button.parents("tr")).remove().draw();
                                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                                        }, true]
                                    ]
                                });
                            }
                            else {

                                var errores = data.errormesages != null ? data.errormesages.join('\n') : ' Error! No Se pudo relizar la operación'
                                iziToast.error({
                                    title: 'Mantenedor Managers',
                                    message: errores,
                                    overlay: true,
                                    displayMode: 'once',
                                    position: 'center',
                                    progressBarColor: 'rgb(0, 255, 184)',
                                    timeout: 5000,
                                    buttons: [
                                        ['<button>Ok</button>', function (instance, toast) {
                                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                                        }, true]
                                    ]
                                });


                            }

                        }

                    });

                }, true],
                ['<button>No</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }],
            ]
        });
    });

});