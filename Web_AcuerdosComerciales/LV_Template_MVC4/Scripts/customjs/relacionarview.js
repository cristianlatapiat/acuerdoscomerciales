﻿$(document).ready(function () {
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });


    var fecha = $('#txtFecha');
    if (fecha.val() != "") {
        let f = fecha.val().substring(0, 10);
        fecha.val(f);
    };

    // se agrega la funcionalidad de datatable
    var tabla = $("#gridRelacion").DataTable({
        lengthMenu: [[20, 40, 60], [20, 40, 60]],
        order: [],
        pageLength: 20,
        language: {
            url: "./Scripts/Spanish.json"
        },
        columnDefs: [],
       
        drawCallback: function () {
            $('[data-toggle="tooltip"]').tooltip();
        }
    });

    var isAcuerdo = $(".isAcuerdo");
    var isRebate = $(".isRebate");
    
    $("#ddlTiposRebates").change(function () {
        var rebate = $(this).val();

        if (rebate == "V") {
            isRebate.hide();
            isAcuerdo.show();
            $("#txtRebate").val("");
        }
        else if (rebate == "F") {
            isRebate.show();
            isAcuerdo.hide();
            $("#txtAcuerdo").val("");

        }
        else {
            isRebate.hide();
            $("#txtRebate").val("");

            isAcuerdo.hide();
            $("#txtAcuerdo").val("");
        }
    });


    $("#ddlTiposRebates").trigger("change");

    $("#ddlManagers").change(function () {
        $("#ddlManagers").css('border', '1px solid #ccc');
        document.getElementById('mensajeManagers').style.display = 'none';
    });

    //Limpiar mensaje de error
    $("#txtFecha").focus(function () {
        $("#txtFecha").css('border', '1px solid #ccc');
        document.getElementById('mensajeFecha').style.display = 'none';
    });
    $("#btnRelacionar").click(function () {
        var ingresar = true;
        var manager = $("#ddlManagers").val();
        if (manager == "")
        {
            document.getElementById('mensajeManagers').style.display = 'block';
            $("#ddlManagers").css('border', '1px solid #b94a48');
            ingresar = false;
        }
        if (fecha.val() == "") {
            document.getElementById('mensajeFecha').style.display = 'block';
            fecha.css('border', '1px solid #b94a48');
            ingresar = false;
        }
        if (ingresar)
        {
            $.ajax({
                type: "POST",
                url: "/Acuerdos/Relacionar",
                data: $("#formRelacionar").serialize(),
                success: function (data) {

                    if (data.data === "OK") {

                        iziToast.success({
                            icon: 'far fa-check-circle',
                            title: 'Mantenedor Managers-Fondo',
                            message: 'Registro ingresado correctamente!',
                            position: 'center',
                            overlay: true,
                            displayMode: 'once',
                            position: 'center',
                            progressBarColor: 'rgb(0, 255, 184)',
                            timeout: 5000,
                            buttons: [
                                ['<button>Ok</button>', function (instance, toast) {

                                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                    window.location.href = "/Acuerdos/Index";

                                }, true],

                            ]
                        });

                    } else {
                        var errores = data.errormesages != null ? data.errormesages.join('\n') : ' Error! No Se pudo relizar la operación'
                        iziToast.error({
                            title: 'Mantenedor Managers-Fondo',
                            message: errores,
                            overlay: true,
                            displayMode: 'once',
                            position: 'center',
                            progressBarColor: 'rgb(0, 255, 184)',
                            timeout: 5000,
                            buttons: [
                                ['<button>Ok</button>', function (instance, toast) {
                                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                }, true],
                            ]
                        });
                    }
                }
            });
        }

    });
       
});