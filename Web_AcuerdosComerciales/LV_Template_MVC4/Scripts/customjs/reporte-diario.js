﻿$(document).ready(function () {

    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    var groupColumn = 0;
    // se agrega la funcionalidad de datatable
    var tabla = $("#gridReporteDiario").DataTable({
        lengthMenu: [[20, 40, 60], [20, 40, 60]],
        pageLength: 20,
        language: {
            url: "../Scripts/Spanish.json"
        },
        columnDefs: [
           { "visible": false, "targets": groupColumn },
           { "visible": false, "targets": 1 },
           { "visible": false, "targets": 2 },
           { "visible": false, "targets": 3 },
           { "visible": false, "targets": 4}
        ],
        order: [[groupColumn, 'asc']],
        //para usar los botones exportar
        responsive: "true",
        dom: 'Bfrtilp',
        buttons: [
			{
			    extend: 'excelHtml5',
			    exportOptions: {
			        columns: [0, 2, 3,4,8,9,10],
			        format: {
			            body: function (data, row, column, node) {
			                var formatColum = [1,2,3,5,6];
			                
			                if (formatColum.indexOf(column) > 0) {
			                    return data.replace(',', '.');
			                }

			                return data;
			            }
			        }

			    },
			    text: '<i class="fas fa-file-excel"></i> Excel',
			    titleAttr: 'Exportar a Excel',
			    className: 'btn btn-outline-success',
			    init: function (api, node, config) {
			        $(node).removeClass('dt-button')
			    }
			}
        ],
        drawCallback: function (settings) {
            $('[data-toggle="tooltip"]').tooltip();
            var api = this.api();
            var rows = api.rows({ page: 'current' }).nodes();
            var last = null;
            var rowData = rows.data();//[0][4];
            var totalManagerFree = 0;
            var totalRebate = 0;
            var managerFree = "";
            var rebate = "";
            api.column(groupColumn, { page: 'current' }).data().each(function (group, i) {
                if (last !== group) {
                    var arr = rowData.filter(function (value, index) { return value[0] == group; })[0];
                    totalManagerFree = arr[1];
                    totalRebate = arr[2];
                    managerFree = arr[3] != "" ? arr[3] +" %":"";
                    rebate = arr[4] != "" ? arr[4] + " %" : "";;
                    $(rows).eq(i).before(
                        '<tr class="group"><td>' + group + '</td><td align="right">' + managerFree + '</td><td align="right">' + rebate + '</td><td>TOTAL</td></td><td align="right">' + totalManagerFree + '</td><td align="right">' + totalRebate + '</td></tr>'
                    );

                    last = group;
                }
            });
        }
    });

    function IsNumeric(input) {
        return (input - 0) == input && ('' + input).trim().length > 0;
    }
    //Limpiar mensaje de error

    $("#ddlPeriodos").change(function () {
        $("#ddlPeriodos").css('border', '1px solid #ccc');
        document.getElementById('mensajePeriodo').style.display = 'none';
    });

    $("#ddlManagers").change(function () {
        $("#ddlManagers").css('border', '1px solid #ccc');
        document.getElementById('mensajeManager').style.display = 'none';
    });

    function ValidaBusqueda() {
        var ingresar = true;

        var periodo = $("#ddlPeriodos").val();
        var manager = $("#ddlManagers").val();

        if (periodo === "") {
            document.getElementById('mensajePeriodo').style.display = 'block';
            $("#ddlPeriodos").css('border', '1px solid #b94a48');
            ingresar = false;
        }

        if (manager === "") {
            document.getElementById('mensajeManager').style.display = 'block';
            $("#ddlManagers").css('border', '1px solid #b94a48');
            ingresar = false;
        }

        return ingresar;
    }

    $("#btnBuscar").click(function () {
        var isValid = ValidaBusqueda();
        if (isValid) {
            //document.getElementById("formRelacionar").submit();
            $("#formReporteDiario").submit();
        }

    });

});