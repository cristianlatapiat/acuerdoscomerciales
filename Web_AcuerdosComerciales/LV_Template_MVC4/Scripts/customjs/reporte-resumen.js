﻿$(document).ready(function () {

    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    var groupColumn = 0;
    // se agrega la funcionalidad de datatable
    var tabla = $("#gridReporte").DataTable({
        lengthMenu: [[20, 40, 60], [20, 40, 60]],
        pageLength: 20,
        language: {
            url: "../Scripts/Spanish.json"
        },
        columnDefs: [
            { "visible": false, "targets": groupColumn },
            { "visible": false, "targets": 2 },
            { "visible": false, "targets": 3 }
        ],
        order: [[groupColumn, 'asc']],
        //para usar los botones exportar
        responsive: "true",
        dom: 'Bfrtilp',
        buttons: [
			{
			    extend: 'excelHtml5',
			    customize: function (xlsx, row) {
			        var sheet = xlsx.xl.worksheets['sheet1.xml'];

			        // $('row c[r^="D"]', sheet).attr('s', 63);
			        //$('row c[r^="G"]', sheet).attr('s', '64');

			    },
			    exportOptions: {
			        columns: [0, 4, 5, 6, 7, 8, 9],
			        format: {
			            body: function (data, row, column, node) {
			                var formatColum = [2, 3, 5, 6];

			                if (formatColum.indexOf(column) > 0) {
			                    return data.replace(',', '.');
			                }

			                return data;
			            }
			        }

			    },
			    text: '<i class="fas fa-file-excel"></i> Excel',
			    titleAttr: 'Exportar a Excel',
			    className: 'btn btn-outline-success',
			    init: function (api, node, config) {
			        $(node).removeClass('dt-button')
			    }
			}
        ],
        drawCallback: function (settings) {
            $('[data-toggle="tooltip"]').tooltip();
            var api = this.api();
            var rows = api.rows({ page: 'current' }).nodes();
            var last = null;
            var rowData = rows.data();
            var managerFree = 0;
            var rebate = 0;
            api.column(groupColumn, { page: 'current' }).data().each(function (group, i) {
                if (last !== group) {
                    var arr = rowData.filter(function (value, index) { return value[0] == group; })[0];
                    managerFree = arr[2];
                    rebate = arr[3];
                    $(rows).eq(i).before(
                        '<tr class="group"><td colspan="4">' + group + '</td><td>TOTAL</td><td align="right">' + managerFree.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + '</td></td><td align="right">' + rebate.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + '</td></tr>'
                    );

                    last = group;
                }
            });
        }

    });


    //Limpiar mensaje de error

    $("#ddlPeriodos").change(function () {
        $("#ddlPeriodos").css('border', '1px solid #ccc');
        document.getElementById('mensajePeriodo').style.display = 'none';
    });

    function ValidaBusqueda() {
        var ingresar = true;

        var periodo = $("#ddlPeriodos").val();

        if (periodo === "") {
            document.getElementById('mensajePeriodo').style.display = 'block';
            $("#ddlPeriodos").css('border', '1px solid #b94a48');
            ingresar = false;
        }


        return ingresar;
    }

    $("#btnBuscar").click(function () {
        var isValid = ValidaBusqueda();
        if (isValid) {
            //document.getElementById("formRelacionar").submit();
            $("#formReporte").submit();
        }

    });

});