﻿//Checkar rut si es valido
function FormatoRut(rut) {
    // Despejar Puntos
    var valor = rut.value.replace('.', '');
    // Despejar Guión
    valor = valor.replace('-', '');

    // Aislar Cuerpo y Dígito Verificador
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();

    // Formatear RUN
    rut.value = cuerpo + '-' + dv

}

//Validar solo ingreso de numeros
function SoloNumeros(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 100 || charCode != 190)) {
        return false;
    }
    return true;
}


function validateQty(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 45 && charCode != 8 && (charCode != 46) && (charCode < 48 || charCode > 57))
        return false;
    if (charCode == 46) {
        if ((el.value) && (el.value.indexOf('.') >= 0))
            return false;
        else
            return true;
    }
    return true;
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = evt.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
}


function numerosDecimales(evt, obj) {

    var charCode = (evt.which) ? evt.which : event.keyCode
    var value = obj.value;
    var dotcontains = value.indexOf(".") != -1;
    if (dotcontains)
        if (charCode == 46) return false;
    if (charCode == 46) return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

//Ingreso Rut solo numeros y letra K
function IngresoRut(e) {
    var tecla = (document.all) ? e.keyCode : e.which;

    if (tecla === 8 || tecla === 32 || tecla === 192 || tecla === 188 || tecla === 220 || tecla === 50 || tecla === 209 || tecla === 241 || tecla === 13) {
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    var patron = /[0-9Kk]/;
    var tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}


//Comparar Digito verificador para validar rut
function getDV(rut) {
    var numero;
    var rutDV;

    var res = rut.split("-");

    numero = res[0].split("").reverse().join("");
    rutDV = parseInt(res[1]);

    for (i = 0, j = 2, suma = 0; i < numero.length; i++, ((j == 7) ? j = 2 : j++)) {
        suma += (parseInt(numero.charAt(i)) * j);
    }
    n_dv = 11 - (suma % 11);
    var dv = ((n_dv == 11) ? 0 : ((n_dv == 10) ? "K" : n_dv));

    if (rutDV == dv) {
        return true;
    } else {
        return false;
    }
}