﻿using LV_Template_MVC4.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LV_Template_MVC4.Models;
using RestSharp;
using System.Configuration;
using Newtonsoft.Json;
using System.Net;

namespace LV_Template_MVC4.Services
{
    public class ActivosLVServices : IActivosLVServices
    {
        private readonly RestClient client;
        public ActivosLVServices()
        {

            client = new RestClient(ConfigurationManager.AppSettings["WsAcuerdosComerciales"]);
        }
        public IEnumerable<Activos> Get(int id)
        {
            var request = new RestRequest("ACTIVOSLV/LIST/" + id.ToString(), Method.GET);
            var result = client.Execute(request);

            if (result.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<List<Activos>>(result.Content);
            }

            return new List<Activos>();

        }

        public IEnumerable<Activos> GetByCodes(IEnumerable<string> codigos)
        {
            var request = new RestRequest("ACTIVOSLV/LIST", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(codigos);
            var result = client.Execute(request);

            if (result.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<List<Activos>>(result.Content);
            }

            return new List<Activos>();
        }
    }
}