﻿using LV_Template_MVC4.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LV_Template_MVC4.Models;
using RestSharp;
using System.Configuration;
using System.Net;
using Newtonsoft.Json;

namespace LV_Template_MVC4.Services
{
    public class AcuerdosServices : IAcuerdosServices
    {
        private readonly RestClient client;
        public AcuerdosServices()
        {
            client = new RestClient(ConfigurationManager.AppSettings["WsAcuerdosComerciales"]);
        }
        public IEnumerable<Acuerdo> Get(string id = "")
        {
            var request = new RestRequest("ACUERDO/LIST/", Method.GET);
            if (!string.IsNullOrEmpty(id))
                request.AddParameter("id", id);

            var result = client.Execute(request);

            if (result.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<List<Acuerdo>>(result.Content);
            }

            return new List<Acuerdo>();
        }

        public Acuerdo GetById(int id)
        {
            var request = new RestRequest("ACUERDO/VIEW/" + id.ToString(), Method.GET);

            var result = client.Execute(request);

            if (result.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<Acuerdo>(result.Content);
            }

            return null;
        }



        public void Relacionar(AcuerdoList acuerdos)
        {
            var request = new RestRequest("ACUERDO/RELACIONAR/", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(acuerdos);

            var result = client.Execute(request);
            if (result.StatusCode != HttpStatusCode.OK)
            {
                throw new ApplicationException("no fue posible ingresar los acuerdos");
            }


        }

        public IEnumerable<Acuerdo> GetHistoricos(string codigoProducto)
        {
            var request = new RestRequest("ACUERDO/LISTHISTO/" + codigoProducto, Method.GET);

            var result = client.Execute(request);

            if (result.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<List<Acuerdo>>(result.Content);
            }

            return new List<Acuerdo>();
        }

        public void Edit(Acuerdo acuerdo)
        {
            var request = new RestRequest("ACUERDO/EDIT/" + acuerdo.Id, Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(acuerdo);

            var result = client.Execute(request);
            if (result.StatusCode != HttpStatusCode.Created)
            {
                throw new ApplicationException("no fue posible ingresar el acuerdo comercial");
            }
        }
    }
}