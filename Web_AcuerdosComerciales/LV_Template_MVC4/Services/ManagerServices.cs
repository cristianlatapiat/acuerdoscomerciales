﻿using LV_Template_MVC4.Interfaces;
using System;
using System.Collections.Generic;
using LV_Template_MVC4.Models;
using RestSharp;
using System.Net;
using Newtonsoft.Json;
using System.Configuration;

namespace LV_Template_MVC4.Services
{
    public class ManagerServices : IManagerServices
    {
        private readonly RestClient client;

        public ManagerServices()
        {
            client = new RestClient(ConfigurationManager.AppSettings["WsAcuerdosComerciales"]);
        }
        public Manager Add(Manager manager)
        {
            var request = new RestRequest("MANAGER/NEW/", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(manager);

            var result = client.Execute(request);
            if (result.StatusCode == HttpStatusCode.Created)
            {
                return JsonConvert.DeserializeObject<Manager>(result.Content);
            }

            throw new ApplicationException("no fue posible ingresar el nuevo manager");
        }

        public IEnumerable<Manager> Get()
        {
            var request = new RestRequest("MANAGER/LIST/", Method.GET);
            var result = client.Execute(request);

            if (result.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<List<Manager>>(result.Content);
            }

            return new List<Manager>();
        }

        public void Delete(int id)
        {
            var request = new RestRequest("MANAGER/DELETE/" + id.ToString(), Method.DELETE);
            var result = client.Execute(request);

            if (result.StatusCode != HttpStatusCode.OK)
            {
                throw new ApplicationException("No fue posible eliminar el manager solicitado");
            }

        }

        public Manager GetById(int id)
        {
            var request = new RestRequest("MANAGER/VIEW/" + id.ToString(), Method.GET);
            var result = client.Execute(request);

            if (result.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<Manager>(result.Content);
            }

            return null;
        }

        public void Update(Manager manager)
        {
            var request = new RestRequest("MANAGER/EDIT/" + manager.Id, Method.PUT);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(manager);

            var result = client.Execute(request);
            if (result.StatusCode != HttpStatusCode.NoContent)
            {
                throw new ApplicationException("no fue posible actualizar el registro");
            }

            
        }
    }
}