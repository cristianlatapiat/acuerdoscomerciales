﻿using LV_Template_MVC4.Interfaces;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using LV_Template_MVC4.Models;

namespace LV_Template_MVC4.Services
{
    public class ReportServices : IReportServices
    {
        private readonly RestClient client;
        public ReportServices()
        {

            client = new RestClient(ConfigurationManager.AppSettings["WsAcuerdosComerciales"]);
        }



        public IEnumerable<int> GetPeriodos()
        {
            var request = new RestRequest("REPORTE/PERIODOS/", Method.GET);
            var result = client.Execute(request);

            if (result.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<List<int>>(result.Content);
            }

            return new List<int>();
        }

        public IEnumerable<ReporteDiario> GetDiario(string periodo, int idManager, string fondo)
        {
            var request = new RestRequest(string.Format("REPORTE/DIARIOMANFONDO/{0}/{1}", periodo, idManager), Method.GET);
            if (!string.IsNullOrEmpty(fondo))
                request.AddParameter("fondo", fondo);

            var result = client.Execute(request);

            if (result.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<List<ReporteDiario>>(result.Content);
            }

            return new List<ReporteDiario>();
        }

        public IEnumerable<ReporteResumen> GetResumen(string periodo)
        {
            var request = new RestRequest(string.Format("REPORTE/RESUMENMANAGER/{0}", periodo), Method.GET);

            var result = client.Execute(request);

            if (result.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<List<ReporteResumen>>(result.Content);
            }

            return new List<ReporteResumen>();
        }
    }
}