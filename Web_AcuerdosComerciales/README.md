## Web_AcuerdosComerciales
El Mantenedor de acuerdos comerciales, permite establecer una relación comercial con distintos managers, con el objetivo de distribuir los fondos que éstos ofrecen a sus clientes.Esta relación se ve reflejada en un acuerdo, que contiene el acuerdo comercial y de inversión.

### Tecnología

C#.Net Framework 4.0
Visual Studio 2015
Conectada a Backend API Rest

### Compilación

Setear por defecto el proyecto de Web LV.AcuerdosComerciales.Web
Generar solución en modo release de proyecto

### Instalación

Copiar resultado de carpeta ~\bin\Release
hacia Produccion
El archivo de configuración es Web.config, el cual se debe modificar para apuntar a servicios productivos

### Configuración en producción

Configurar archivo Web.config con Bases de datos productivas.

### Autor

* **Cristian Muñoz** - *Larrain Vial*